<?php
// include our OAuth2 Server object
require_once __DIR__.'/server.php';
$db = new Gudang();
require_once __DIR__ . '/vendor/autoload.php';
use Respect\Validation\Validator as v;

$return['timestamp'] = date("Y-m-d H:i:s");
$return['id'] = time();
$return['smshid'] = 0;
$return['status'] = 'AUTH_FAILED';


$request = OAuth2\Request::createFromGlobals();
$response = new OAuth2\Response();
$content = $request->request;

$token = $server->getAccessTokenData(OAuth2\Request::createFromGlobals());

function slog($request, $retval, $desc, $event) {
  global $db;
  global $token;
  $payload = array(
    'token' => $token,
    'filename' => __FILE__,
    'request' => $request,
    'retval' => $retval,
    'desc' => $desc,
    'event' => $event
  );
//  var_dump($payload); die();
  $db->saveLog($payload);
}

// Handle a request for an OAuth2.0 Access Token and send the response to the client
$scopeRequired = 'user';
if (!$server->verifyResourceRequest($request, $response, $scopeRequired)) {
  slog($content, json_encode($response->getParameters()), 'token authorize fail', 'fail');
  // echo "satu";
  $server->getResponse()->send();
  exit();
}

function returnFail($code, $reason = '', $request = null) {
  global $return;
  global $db;
  global $token;
  // var_dump($return);
  switch ($code) {
    case 'OK':
      $return['status'] = $code;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
//      var_dump($request, $return); die();
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
    case 'AUTH_FAILED':
      $return['status'] = $code;
      $return['reason'] = $reason;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
    case 'NO_GROUPNAME':
      $return['status'] = $code;
      $return['reason'] = $reason;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
    case 'NO_DATA':
      $return['status'] = $code;
      $return['reason'] = $reason;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
  }
}

function formatMsisdn($msisdn) {
  if (substr($msisdn, 0, 1) == '8') {
    return $msisdn;
  } elseif (substr($msisdn, 0, 2) == '08') {
    return substr($msisdn, 1);
  } elseif (substr($msisdn, 0, 3) == '628') {
    return substr($msisdn, 2);
  }
}

$queryRespon = "
SELECT DISTINCT
  E.MSGTXT,
  B.SEND_DATE,
  B.SMS_SENT,
  STS.SUCCESS,
  STP.PENDING,
  B.SMS_FAIL,
  BUCKET.REM_QUOTA,
  B.SMS_EXCESS
FROM
  W2SMS_SMS_HEADER B
JOIN  W2SMS_SEND E
  ON B.SMSHID = E.SMSHID
JOIN (
        SELECT SMSHID, COUNT(DECODE(STATUS, 'SUCCESS', 1)) SUCCESS
        FROM W2SMS_SEND
        GROUP BY SMSHID
      ) STS
  ON STS.SMSHID = B.SMSHID
JOIN (
        SELECT SMSHID, COUNT(DECODE(STATUS, 'ON SCHEDULE', 1)) PENDING
        FROM W2SMS_SEND
        GROUP BY SMSHID
      ) STP
  ON STP.SMSHID = B.SMSHID
JOIN (
        SELECT SUB_ID, BUCKET-BUCKET_USAGE REM_QUOTA
        FROM W2SMS_BUCKET_SMS
      ) BUCKET
  ON BUCKET.SUB_ID = B.SUB_ID
JOIN W2SSMS_SUB_COMMUNITY \"USER\"
  ON \"USER\".SUB_ID = B.SUB_ID
WHERE \"USER\".USERLOGIN = '%s'
";




function kirim ($idresult,$commid,$description,$msisdn,$sub_id,$uname,$durasi,$p_type,$waktu_kirim) {
  global $db;
  $dbid = $db->noPdo();

  // $stid = oci_parse($dbid,"begin :r := broadcast_permember_header(:p1,:p2,:p3,:p4,:p5,:p6,:p7,:p8, TO_DATE(:p9, 'DD-MM-YYYY HH24:MI' )); end;");
  //   oci_bind_by_name($stid, ':p1', $idresult);
  //   oci_bind_by_name($stid, ':p2', $commid);
  //   oci_bind_by_name($stid, ':p3', $description);
  //   oci_bind_by_name($stid, ':p4', $msisdn);
  //   oci_bind_by_name($stid, ':p5', $sub_id);
  //   oci_bind_by_name($stid, ':p6', $uname);
  //   oci_bind_by_name($stid, ':p7', $durasi);
  //   oci_bind_by_name($stid, ':p8', $p_type);
  //   oci_bind_by_name($stid, ':p9', $waktu_kirim);
  //
  //   oci_bind_by_name($stid, ':r', $r, 4000);
  //   // $db->dumpToLog($stid);
  //   oci_execute($stid);
  //   oci_free_statement($stid);
  //
  //   return $r;

  // $query = sprintf("begin :r := broadcast_permember_header(2136,'SLO','Hello Wrong :)','8111011199',110200,'tello',1,1,null); end;");
  $query = sprintf("begin :r := broadcast_permember_header(%d,'%s','%s','%s',%d,'%s',%d,%d,to_date('%s', 'DD-MM-YYYY HH24:MI:SS')); end;",
    $idresult,$commid,$description,$msisdn,$sub_id,$uname,$durasi,$p_type,$waktu_kirim);

  echo $query;

  $stid = oci_parse($dbid, $query);
  oci_bind_by_name($stid, ':r', $r, 400);
  oci_execute($stid);
  oci_free_statement($stid);

  return $r;
}

function susunquery ($mode, $param) {

  $mainp = "
  FROM
    W2SMS_SMS_HEADER B
  JOIN  W2SMS_SEND E
    ON B.SMSHID = E.SMSHID
  JOIN (
          SELECT SMSHID, COUNT(DECODE(STATUS, 'SUCCESS', 1)) SUCCESS
          FROM W2SMS_SEND
          GROUP BY SMSHID
        ) STS
    ON STS.SMSHID = B.SMSHID
  JOIN (
          SELECT SMSHID, COUNT(DECODE(STATUS, 'ON SCHEDULE', 1)) PENDING
          FROM W2SMS_SEND
          GROUP BY SMSHID
        ) STP
    ON STP.SMSHID = B.SMSHID
  JOIN (
          SELECT SUB_ID, BUCKET-BUCKET_USAGE REM_QUOTA
          FROM W2SMS_BUCKET_SMS
        ) BUCKET
    ON BUCKET.SUB_ID = B.SUB_ID
  JOIN W2SSMS_SUB_COMMUNITY \"USER\"
    ON \"USER\".SUB_ID = B.SUB_ID
  WHERE \"USER\".USERLOGIN = '%s'
  ";

  switch ($mode) {
    case 'stats':
      $prefix = "
      SELECT
        SUM(B.SMS_SENT) SMS_SENT,
        SUM(STS.SUCCESS) SUCCESS,
        SUM(STP.PENDING) PENDING,
        SUM(B.SMS_FAIL) FAIL,
        BUCKET.REM_QUOTA,
        B.SMS_EXCESS";
      $postfix = "GROUP BY BUCKET.REM_QUOTA, B.SMS_EXCESS";
      $query = sprintf($prefix.$mainp.$postfix, $param);
      // return $query;
    break;
    case 'msg':
      $prefix = "
      SELECT
        TO_CHAR(TO_DATE(B.SEND_DATE),  'YYYY-MM-DD HH24:MI:SS') SEND_DATE,
        E.MSGTXT";
      $postfix = "ORDER BY SEND_DATE";
      $query = sprintf($prefix.$mainp.$postfix, $param);
      // return $query
    break;
  }
  return $query;
}

function url_get_contents($Url) {
    if (!function_exists('curl_init')){
        die('CURL is not installed!');
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $Url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}

function adaGroup($db, $groupname) {
  $query = $db->query(sprintf("SELECT REG_SMS FROM W2SMS_GROUP WHERE REG_SMS LIKE '%%%s%%'", strtoupper($groupname)));
  // var_dump($query);
  if (isset($query[0]['REG_SMS'])) {
    return true;
  } else {
    return false;
  }
}

function getStatus($db, $msisdn) {
  $query = $db->query(sprintf("
    SELECT STATUS FROM W2SMS_CMR_MEMBER WHERE MSISDN = '%s'",
    $msisdn));
//   var_dump($query, $msisdn); die();

  return $query[0]['STATUS'];
}

function gnameToRegsms($db, $group) {
  $gid = $db->query(sprintf("SELECT REG_SMS FROM W2SMS_GROUP WHERE GNAME = '%s'",strtoupper($group)));
  if (isset($gid[0]['REG_SMS'])) {
    return $gid[0]['REG_SMS'];
  } else {
    return false;
  }
}

function insertFail($smshid, $group, $message = '') {
  global $db;
  
  $gid = Util::gidDariNama($group);
  $que = sprintf('SELECT MSISDN FROM W2SMS_GROUP_MEMBER WHERE GID = %d AND STATUS != 1', $gid);
  $result = $db->query($que);
//  var_dump($result); die();
  
//  $dbobj = $db->getDb();
//  $dbobj->beginTransaction();
  foreach($result as $key => $val) {
//    $exe = $dbobj->prepare('
//      INSERT INTO
//      W2SMS_SEND_ERR (SEND_ID, SMSHID, MSISDN, STATUS, ERR_DESC, MSGTXT)
//      VALUES (SEQ_W2SMS_SEND.NEXTVAL, :smshid, :msisdn, :status, :desc, :msgtxt);');
//    $exe->bindparam()
//    var_dump($val, $smshid, $message); die();
    Util::KeepSmsLog($smshid, $val['MSISDN'], 'FAIL', 'Member not active.', $message);
  }
}

function sendToGroup($content) {
  global $db;
  global $return;

  if (adaGroup($db, $content['group_name']) && isset($content['group_name'])) {
    // format broadcast = keyword of W2SMS_KEYWORDS
    // gformat = W2SMS_GROUP','GID',$gid,'REG_SMS
    $gformat = strtoupper(gnameToRegsms($db, $content['group_name']));

    $format_broadcast = $db->query(sprintf("SELECT KEYWORD FROM W2SMS_KEYWORDS WHERE KEYWORD_ID = %d", 1002));
    $format_broadcast = (isset($format_broadcast[0]['KEYWORD'])) ? $format_broadcast[0]['KEYWORD'] : 'CMBROADCAST' ;

    $pass = 0;
    $count = 0;

    $idresult = $db->query("SELECT SEQ_SMS_SCHEDULE_SCHEDULEID.NEXTVAL AS INSERT_ID FROM DUAL");
    $idresult = $idresult[0]['INSERT_ID']+1;

    // $query = $db->query(sprintf("SELECT USERLOGIN, COMMID, SUB_ID, PIC_MSISDN FROM W2SSMS_SUB_COMMUNITY WHERE PIC_MSISDN = '%s' AND USERLOGIN = '%s'", $msisdn, $content['username']));
    $query = $db->query(sprintf("SELECT USERLOGIN, COMMID, SUB_ID, PIC_MSISDN FROM W2SSMS_SUB_COMMUNITY WHERE USERLOGIN = '%s'", $content['username']));
    $sub_id = $query[0]['SUB_ID']; // 110200
    $commid = $query[0]['COMMID'];
    // echo $sub_id;
    // echo $commid;

    $uname = ($content['username'] == $query[0]['USERLOGIN']) ? $content['username'] : $query[0]['USERLOGIN'];
    $durasi = 2;
    $p_type = 2;
    $description = (isset($content['message'])) ? $content['message'] : '';
    $time = time();
    $waktu_kirim = date('d-m-Y H:i', $time);

    // $r = kirim($idresult,$commid,$description,$msisdn,$sub_id,$uname,$durasi,$p_type,$waktu_kirim);
//    $r = kirim($idresult,$commid,$description,$query[0]['PIC_MSISDN'],$query[0]['SUB_ID'],$uname,$durasi,$p_type,$waktu_kirim);

//    if ($r == 'OK') {
      // http://10.2.117.80:8080/commetmo/SmsMO? channel=WEB &adn=2324 &msisdn=$msisdn &msg=$format_broadcast[spasi]#$gformat[spasi]$description
      // production
      // $url = sprintf("http://10.1.89.201:8081/commetmo/SmsMO?channel=WEB&adn=2324&msisdn=62%s&msg=%s #%s %s", $query[0]['PIC_MSISDN'], $format_broadcast, $gformat, $description);
      
      $que = sprintf("INSERT INTO W2SMS_MESSAGE_TEMP(SMSHID,MSGTXT,CREATE_DATE) VALUES (%d,'%s',SYSDATE)", $idresult, $description);
      $db->query($que, false);

      // development
      // $broadcast		= $format_broadcast.'#'.$gformat.'#'.$description;
      $url = sprintf("http://10.2.117.80:8080/commetmo/SmsMO?channel=WEB&adn=2324&msisdn=%s&msg=%s #%s %s", $query[0]['PIC_MSISDN'], $format_broadcast, $gformat, $idresult);
      // echo "http://10.2.117.80:8080/commetmo/SmsMO?channel=SMS&adn=2323&msisdn=8111011199&msg=CMBROADCAST%20#IMAD%20Test\n";
      $url = str_replace(" ", '%20', $url);
      $url = str_replace("#", '%23', $url);
//       echo $url."\n";
        $sent = url_get_contents($url);  // NOTE comment this on dev server
//      $sent = true; // NOTE uncomment for development phase
//      var_dump($sent == 1); die();
      if ($sent > 0) {
        insertFail($idresult, $content['group_name'], $description);
//        echo "sent"; die();
        if (strlen($description) > 350) {
          $return['message'] = substr($description, 0, 125).'... Content omitted ...';
          $content['message'] = $return['message'];
        } else {
          $return['message'] = $description;
        }
        $return['sent_date'] = date('Y-m-d H:i:s', $time);
//        $return['id'] = $idresult;

        // HACK: beware of race condition by put $idresult here.
        $return['smshid'] = $idresult;
        returnFail('OK','',$content);
      } else {
        returnFail('NO_DATA', '', $content);
      }
//    } else {
//      returnFail('NO_DATA', '', $content);
//    }
  } else {
    returnFail('NO_GROUPNAME', 'Group name missing or not recognized', $content);
  }
}

function inputs() {
  return array(
    'group_name' => v::alnum()->noWhitespace()->length(1,20),
    'message' => v::alnum(",.?'\"!()[]")->length(1, 1001)
  );
}

function validate($input) {
  $c = array();
  $v = inputs();

  foreach (array_keys($v) as $k) {
    if (isset($input[$k])) {
      if ($v[$k]->validate($input[$k])) {
        $c[$k] = $input[$k];
        slog('validasi field '.$k, 'true', 'Format benar', 'success');
      } else {
        //$c[$k] = '';
        slog('validasi field '.$k, 'false', 'Format salah', 'fail');
      }
    }
  }

  return $c;
}

$content = validate($content);

if (isset($content['message'])) {
  // NOTE: 14 march 2017: no more username check
  $content['username'] = $token['user_id'];
  sendToGroup($content);
} else {
  returnFail('ERROR', 'Message contain invalid character.', $content);
}
