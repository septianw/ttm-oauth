<?php
// include our OAuth2 Server object
require_once __DIR__.'/server.php';
$db = new Gudang();
require_once __DIR__ . '/vendor/autoload.php';
use Respect\Validation\Validator as v;
// require_once __DIR__.'/addclient.php';

$request = OAuth2\Request::createFromGlobals();
$response = new OAuth2\Response();

function keLogout() {
    $uri = base64_encode(getUri());
    header('Location: apps/user.php?q=logout&to='.$uri);
}

function keLogin() {
    $uri = getUri();
    // var_dump($_SESSION);
    // var_dump($uri);
    header('Location: apps/login.php?to='.base64_encode($uri));
}

function getUri() {
  global $request;
  $protocol = (!empty($request->server['HTTPS']) && $request->server['HTTPS'] !== 'off' || $request->server['SERVER_PORT'] == 443) ? "https://" : "http://";
  $domainName = $request->server['HTTP_HOST'];
  $uri = $protocol.$domainName.$request->server['REQUEST_URI'];
  return $uri;
}


$uri = '';

function comeIn($clientid, $q) {
  $uri = strtok(getUri(),'?');
  $query = $q;
  $query['client_id'] = $clientid;
  // echo http_build_query($query)."\n";
  header('Location: '.$uri.'?'.http_build_query($query));
}

// validate the authorize request
// validasi request, kalau client id kosong, maka buat baru. kalau ada langsung masuk.
if (!$server->validateAuthorizeRequest($request, $response)) {
  // var_dump($request);
  if ((empty($request->query['client_id']) && isset($request->query['client_id'])) &&
      (!empty($request->query['redirect_uri']) && isset($request->query['redirect_uri']))) {
    session_start();
    if (empty($request->query['client_id']) && isset($_SESSION['username'])) {
      $username = $_SESSION['username'];
      $query = sprintf("SELECT CLIENT_ID FROM OAUTH_CLIENTS WHERE REDIRECT_URI = '%s' AND USER_ID = '%s'",
      $request->query['redirect_uri'], $username);
      $result = $db->query($query);
      if (isset($result[0]['CLIENT_ID'])) {
        // echo 'masuk';
        // var_dump($result);
        $db->dumpToLog(__LINE__, 'Client id found, Native API logging in with '.$result[0]['CLIENT_ID']);
        comeIn($result[0]['CLIENT_ID'], $request->query);
      } else {
        $uri = explode('/', $request->query['redirect_uri']);
        $uri = $uri[2];
        $data = array(
          'username' => $username,
          'redirect_uri' => $request->query['redirect_uri'],
          'app_name' => $uri
        );
        $hasil = add_app($data);
        if ($hasil['status'] == true) {
          // echo 'baru';
          $db->dumpToLog(__LINE__, 'Client id created, Native API logging in with '.$hasil['result']['CLIENT_ID']);
          comeIn($hasil['result']['CLIENT_ID'], $request->query);
        }
      }
    } else {
      keLogin();
    }
  } else {
    $response->send();
    exit();
  }

}

function susunqinsert($table, $field) {
  $q = "INSERT INTO ".$table." (";
  $c = 1;
  // insert key
  foreach($field as $key => $value) {
    if ($c == count($field)) {
      $q .= $key.') ';
    } else {
      $q .= $key.', ';
    }
    $c++;
  }

  $q .= "VALUES (";
  $c = 1;
  foreach($field as $key => $value) {
    if ($c == count($field)) {
      $q .= "'".$value."') ";
    } else {
      $q .= "'".$value."', ";
    }
    $c++;
  }

  return $q;
}

// tambahkan aplikasi baru berdasarkan redirect URI
function add_app($data) {
 
  $db = new Gudang(); 
  // error_log(serialize($db)); 
  $clientsecret = $db->getrand();
  $clientid = $db->getrand();

  $field = array(
    'CLIENT_ID' => $clientid,
    'CLIENT_SECRET' => $clientsecret,
    'SCOPE' => 'user',
    // 'CREATED_BY' => $token['user_id'].'-'.$token['client_id']
    'CREATED_BY' => 'native API'
  );

  $field['USER_ID'] = $data['username'];
  $field['REDIRECT_URI'] = $data['redirect_uri'];
  if (isset($data['app_name']) && !empty($data['app_name'])) { $field['NAME'] = $data['app_name']; }
  $query = susunqinsert('OAUTH_CLIENTS', $field);

  if ($db->query($query, false)) {
    return array(
      'status' => true,
      'result' => $field
    );
  }
}


// get user id from client_id
$query = sprintf("SELECT USER_ID FROM OAUTH_CLIENTS WHERE CLIENT_ID = '%s'", $request->query['client_id']);
$db->dumpToLog(__LINE__, $query);
$userid = $db->query($query);

// get application name from client id
$query = sprintf("SELECT \"NAME\" FROM OAUTH_CLIENTS WHERE CLIENT_ID = '%s'", $request->query['client_id'] );
$db->dumpToLog(__LINE__, $query);
$appname = $db->query($query);


if (isset($userid[0]['USER_ID'])) {
  session_start();
  if (!isset($_SESSION['username']) || (isset($_SESSION['username']) != isset($userid[0]['USER_ID']))) {
    keLogin();
  } elseif (($_SESSION['username']) != ($userid[0]['USER_ID'])) {
    keLogout();
  } else {
    if (empty($_POST)) {
      exit(sprintf('<!doctype html><html lang=""> <head> <meta charset="utf-8"> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <meta name="viewport" content="width=device-width, initial-scale=1"> <meta name="description" content="Teman telkomsel OAuth2 API Authorization page."> <meta name="author" content="Septian Wibisono"> <link rel="shortcut icon" href="apps/favicon.ico"> <style>html, body{height:100%%; margin: 0; padding: 0; overflow-x: hidden; font-family: "Wire One", sans-serif;}body{background: #fff !important;}.login-container{/*border:solid 1px #ddd;*/ margin: 10%% auto; /*padding: 110px 40px;*/ /*box-shadow: 0 3px 15px -6px #000;*/}.login-box{/*background-color: rgba(150,150,150, 0.1);*/ background-color: #fff; height: 100%%; border-radius: 4px; padding: 30px 5px 30px 5px; box-shadow: 0 3px 15px -6px #000;}.login-head{padding: 0px 10px 2px 10px; text-align: center;}.login-head > img{width: 30%%;}.login-title{color: #000; padding: 10px; margin-top: 20px; font-size: 13.5px; font-weight: bold;}.login-title2{color: #000; padding: 10px; margin-top: 0px; font-size: 13.5px; font-weight: bold;}.login-body{padding: 5px 30px;}.baciro-input-line{padding: 3px; margin-top: 5px;}#bg{position: fixed; top: 0; left: 0; /* Preserve aspet ratio */ min-width: 100%%; min-height: 100%%;}ul{/* background: #3399ff;*/ padding: 0px 20px; list-style:none;}ul li{/*background: #cce5ff;*/ margin: 5px;}ul li i{margin-right: 10px; color: #00bd00;}.panel-footer2{padding: 10px 15px; border-bottom-right-radius: 3px; border-bottom-left-radius: 3px;}</style> <link rel="stylesheet" href="apps/css/font-awesome.min.css"> <link rel="stylesheet" href="apps/css/bootstrap.min.css"> <link href="https://fonts.googleapis.com/css?family=Wire+One" rel="stylesheet"> <title>Teman Telkomsel</title> </head> <body style="overflow-y: hidden;"> <img src="apps/images/bg7.png" id="bg" alt=""> <div class="page-content"> <div class="row"> <div class="col-sm-4"></div><div class="col-sm-4"> <div class="login-container"> <div class="login-box"> <div class="login-head"> <img src="apps/images/logo_kumis.png"/> </div><div class="login-body"> <div class="login-title"> <p>Selamat Datang %s</p><p>Aplikasi %s ingin</p></div><div class="baciro-input-line"> <ul> <li><i class="fa fa-check-square" aria-hidden="true"></i> Akses ke member</li><li><i class="fa fa-check-square" aria-hidden="true"></i> Akses ke Group</li><li><i class="fa fa-check-square" aria-hidden="true"></i> Kirim Broadcast</li><li><i class="fa fa-check-square" aria-hidden="true"></i> Akses Quota</li><li><i class="fa fa-check-square" aria-hidden="true"></i> Akses History</li></ul> </div><div class="login-title2">Apakah anda mengijinkan aplikasi %s untuk mengakses akun Teman telkomsel anda? </div><form method="post"> <div class="panel-footer2"> <div class="clearfix"> <input class="btn btn-default pull-left" value="Tidak" name="authorized" type="submit"> <input class="btn btn-primary pull-right" value="Ya" name="authorized" type="submit"> </div></div></form> </div></div></div><div class="col-sm-4"></div></div></div></body> <html>',
        $userid[0]['USER_ID'], $appname[0]['NAME'], $appname[0]['NAME'], '', ''));
    }
    // display an authorization form
    // Working authorization form
//    if (empty($_POST)) {
//      exit(sprintf('
//    <form method="post">
//		<link rel="stylesheet" href="http://10.2.117.80:8800/web2sms/template/kumis/assets/css/kumis.css">
//		<img src="http://10.2.117.80:8800/web2sms/template/kumis/images/logo_kumis.png"  style="float:left;"/>
//		<label>Do You Authorize %s application to access your Teman Telkomsel resource ?</label>
//		<!--
//		<input type="submit" name="authorized" value="yes">
//		<input type="submit" name="authorized" value="no">
//		-->
//		<input type="submit" class="btn btn-primary btn-login" value="yes" name="authorized">
//		<input type="submit" class="btn btn-primary btn-login" value="no" name="authorized">
//    </form>', $appname[0]['NAME']));
//    }

    // print the authorization code if the user has authorized your client
    $is_authorized = ($_POST['authorized'] === 'Ya');
    $server->handleAuthorizeRequest($request, $response, $is_authorized,$userid[0]['USER_ID']);
    if ($is_authorized) {
      // hancurkan session bila user menekan yes
      session_unset();
      session_destroy();
      echo $response->getHttpHeader('Location');
      // this is only here so that you get to see your code in the cURL request. Otherwise, we'd redirect back to the client
      // $code = substr($response->getHttpHeader('Location'), strpos($response->getHttpHeader('Location'), 'code=')+5, 40);
      // exit("SUCCESS! Authorization Code: $code");
    }
    $response->send();
  }
}
