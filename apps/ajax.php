<?php

require_once __DIR__.'/../server.php';
require_once __DIR__ . '/../vendor/autoload.php';
use Respect\Validation\Validator as v;

$db = new Gudang();

function slog($request, $retval, $desc, $event) {
  global $db;
  $payload = array(
    'token' => null,
    'filename' => __FILE__,
    'request' => $request,
    'retval' => $retval,
    'desc' => $desc,
    'event' => $event
  );
  $db->saveLog($payload);
}

function validate($input, $rule) {
  $c = array();
  $v = $rule;

  foreach (array_keys($v) as $k) {
    if (isset($input[$k])) {
      if ($v[$k]->validate($input[$k])) {
        $c[$k] = $input[$k];
        slog('validasi field '.$k, 'true', 'Format benar', 'success');
      } else {
        //$c[$k] = '';
        slog('validasi field '.$k, 'false', 'Format salah', 'fail');
      }
    }
  }

  return $c;
}

// $content = validate($content);

function delete_row($clientid, $clientsecret) {
  global $db;
  require 'session.php';
  if (!session_valid()) { header('Location: login.php'); }
  $query = "DELETE FROM OAUTH_CLIENTS WHERE CLIENT_ID = :clientid AND CLIENT_SECRET = :clientsecret AND USER_ID = :userid";

  $bindparam = array(
    array( ':clientid', $clientid, PDO::PARAM_STR, 80),
    array( ':clientsecret', $clientsecret, PDO::PARAM_STR, 80),
    array( ':userid', $_SESSION['username'], PDO::PARAM_STR, 80),
  );
  return $db->query($query,false,$bindparam);
}

function susunqinsert($table, $field) {
  $q = "INSERT INTO ".$table." (";
  $c = 1;
  // insert key
  foreach($field as $key => $value) {
    if ($c == count($field)) {
      $q .= $key.') ';
    } else {
      $q .= $key.', ';
    }
    $c++;
  }

  $q .= "VALUES (";
  $c = 1;
  foreach($field as $key => $value) {
    if ($c == count($field)) {
      $q .= "'".$value."') ";
    } else {
      $q .= "'".$value."', ";
    }
    $c++;
  }

  return $q;
}

function add_app($data) {
  global $db;
  require 'session.php';
  if (!session_valid()) { header('Location: login.php'); }
  session_start();

  $clientsecret = $db->getrand();
  $clientid = $db->getrand();

  $field = array(
    'CLIENT_ID' => $clientid,
    'CLIENT_SECRET' => $clientsecret,
    'USER_ID' => $_SESSION['username'],
    'SCOPE' => 'apps user',
    'CREATED_BY' => $_SESSION['username'],
    'CREATED_DATE' => date('d-M-Y h:i:s.u A', time())
  );

  if (isset($data['uri']) && !empty($data['uri'])) { $field['REDIRECT_URI'] = $data['uri']; } else {
    $_SESSION['message'] = array(
      'type' => 'danger',
      'content' => 'URL hanya boleh mengandung karakter alphanumeric dan karakter ":/."'
    );
    header('Location: addapp.php');
    exit;
  }
  if (isset($data['name']) && !empty($data['name'])) { $field['NAME'] = $data['name']; } else {
    $_SESSION['message'] = array(
      'type' => 'danger',
      'content' => 'Nama aplikasi tidak boleh kosong, dan hanya boleh alphanumeric'
    );
    header('Location: addapp.php');
    exit;
  }

  $query = susunqinsert('OAUTH_CLIENTS', $field);

//  var_dump($query); die();

  if ($db->query($query, false)) {
    header('Location: dashboard.php');
  }
}

function edit_row($data) {
  global $db;
  require 'session.php';
  if (!session_valid()) { header('Location: login.php'); }
  // session_start();

  $query = "SELECT * FROM OAUTH_CLIENTS WHERE CLIENT_ID = :dataid AND CLIENT_SECRET = :datasecret AND USER_ID = :userid";

  $bindparam = array(
    array( ':dataid', $data['CLIENT_ID'], PDO::PARAM_STR, 80),
    array( ':datasecret', $data['CLIENT_SECRET'], PDO::PARAM_STR, 80),
    array( ':userid', $_SESSION['username'], PDO::PARAM_STR, 80)
  );

  // var_dump($query);
  if ($data = $db->query($query,true,$bindparam)) {
    // var_dump($data);
    $_SESSION['datauser'] = $data;
  }
}

function update_app($data) {
  global $db;
  require 'session.php';
  if (!session_valid()) { header('Location: login.php'); }
  // session_start();

  if (isset($data['uri']) && !empty($data['uri'])) { $field['REDIRECT_URI'] = $data['uri']; } else {
    $_SESSION['message'] = array(
      'type' => 'danger',
      'content' => 'URL hanya boleh mengandung karakter alphanumeric dan karakter ":/."'
    );
    header('Location: addapp.php');
  }
  if (isset($data['name']) && !empty($data['name'])) { $field['NAME'] = $data['name']; } else {
    $_SESSION['message'] = array(
      'type' => 'danger',
      'content' => 'Nama aplikasi tidak boleh kosong'
    );
    header('Location: addapp.php');
  }

  $query = "UPDATE OAUTH_CLIENTS SET NAME = :dataname, REDIRECT_URI = :datauri WHERE CLIENT_ID = :clientid AND CLIENT_SECRET = :clientsecret";  
  $bindparam = array(
    array( ':dataname', $data['name'], PDO::PARAM_STR, 225),
    array( ':datauri', $data['uri'], PDO::PARAM_STR, 2000),
    array( ':clientid', $_SESSION['datauser'][0]['CLIENT_ID'], PDO::PARAM_STR, 80),
    array( ':clientsecret', $_SESSION['datauser'][0]['CLIENT_SECRET'], PDO::PARAM_STR, 80)
  );
  // var_dump($query);
  if ($db->query($query,false,$bindparam)) {
    unset($_SESSION['datauser']);
    header('Location: dashboard.php');
  }
}

// var_dump($_POST);

switch ($_POST['function']) {
  case 'delete_row':
    return delete_row($_POST['CLIENT_ID'], $_POST['CLIENT_SECRET']);
  break;
  case 'add_app':
    // TODO: :sql inject: issue di file server.php:199 bersumber dari sini.
    $rule = array(
      'uri' => v::alnum(':/.')->noWhitespace()->length(1,2000),
      'name' => v::alnum()->notEmpty()->length(1,225)
    );
    $data = validate($_POST, $rule);
    return add_app($data);
  break;
  case 'update_app':
    $rule = array(
      'uri' => v::alnum('://.')->noWhitespace()->length(1,2000),
      'name' => v::notEmpty()->length(1,225)
    );
    $data = validate($_POST, $rule);
    return update_app($data);
  break;
  case 'edit_row':
    $rule = array(
      'CLIENT_ID' => v::alnum()->noWhitespace()->length(1, 80),
      'CLIENT_SECRET' => v::alnum()->noWhitespace()->length(1, 80)
    );
    $data = validate($_POST, $rule);
    edit_row($data);
  break;
  default:
//    var_dump($_POST);
}

