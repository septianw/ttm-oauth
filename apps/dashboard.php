<?php
  require_once 'session.php';
  // session_start();
  if (!session_valid()) { header('Location: login.php'); }
  // if (!isset($_SESSION['username'])) { header('Location: login.php'); }

  // var_dump($_SESSION);
  unset($_SESSION['datauser']);

  require_once __DIR__.'/../server.php';

  $db = new Gudang();

  $query = sprintf("SELECT NAME, CLIENT_ID, CLIENT_SECRET, REDIRECT_URI, CREATED_DATE FROM OAUTH_CLIENTS WHERE USER_ID = '%s'  ORDER BY CREATED_DATE DESC", $_SESSION['username']);
  $data = $db->query($query);
?>

<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Septian Wibisono">
    <meta http-equiv="refresh" content="300" >
    <link rel="shortcut icon" href="http://10.2.117.80:8800/web2sms/template/kumis/assets/ico/favicon.ico">

    <!-- Font awesome CSS -->
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <title>Teman telkomsel oauth dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script><style type="text/css"></style>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <style id="holderjs-style" type="text/css"></style></head>

  <body>

    <?php
      if (isset($_SESSION['message'])) {
        switch ($_SESSION['message']['type']) {
          case 'success':
            echo '<div class="alert alert-success" role="alert">'.$_SESSION['message']['content'].'</div>';
            unset($_SESSION['message']);
          break;
          case 'warning':
            echo '<div class="alert alert-warning" role="alert">'.$_SESSION['message']['content'].'</div>';
            unset($_SESSION['message']);
          break;
          case 'info':
            echo '<div class="alert alert-info" role="alert">'.$_SESSION['message']['content'].'</div>';
            unset($_SESSION['message']);
          break;
          case 'danger':
            echo '<div class="alert alert-danger" role="alert">'.$_SESSION['message']['content'].'</div>';
            unset($_SESSION['message']);
          break;
        }
      }
    ?>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="dashboard.php">Teman telkomsel</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="./addapp.php"><i class="fa fa-plus fa-lg"></i> Add application</a></li>
            <li><a href="./user.php?q=logout"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
            <!-- <li><a href="dashboard.php">Profile</a></li>
            <li><a href="dashboard.php">Help</a></li> -->
          </ul>
          <!-- <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form> -->
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="main">

          <h2 class="sub-header">Registered application</h2>
          <div id="flip-scroll" class="table-responsive">
            <table id="table-app" class="table table-striped">
              <thead>
                <tr>
                  <th>Client name</th>
                  <th>Client id</th>
                  <th>Client secret</th>
                  <th>Redirect URL</th>
                  <th>Created date</th>
                  <th>Delete</th>
                  <th>Modify</th>
                </tr>
              </thead>
              <tbody>
              <?php
                foreach ($data as $key => $value) {
                  echo '<tr>';
                    printf('<td>%s</td>', htmlentities($value['NAME']));
                    printf('<td>%s</td>', htmlentities($value['CLIENT_ID']));
                    printf("<td>%s</td>", htmlentities($value['CLIENT_SECRET']));
                    printf("<td>%s</td>", htmlentities($value['REDIRECT_URI']));
                    printf("<td>%s</td>", DateTime::createFromFormat("d-M-y H.i.s.u A", $value['CREATED_DATE'])->format('d F Y h:i:s'));
                    printf('<td><button type="button" class="btn btn-xs btn-danger" onclick="hapus(%d)">Delete</button></td>', $key+1);
                    printf('<td><button type="button" class="btn btn-xs btn-warning" onclick="ubah(%d)">Modify</button></td>', $key+1);
                  echo '</tr>';
                }
              ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <script src="js/dashboard.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
