<?php
require 'session.php';
require_once __DIR__.'/../server.php';
require_once __DIR__ . '/../vendor/autoload.php';
use Respect\Validation\Validator as v;

$db = new Gudang();
$token = null;


function slog($request, $retval, $desc, $event) {
  global $db;
  global $token;
  $payload = array(
    'token' => $token,
    'filename' => __FILE__,
    'request' => $request,
    'retval' => $retval,
    'desc' => $desc,
    'event' => $event
  );
  $db->saveLog($payload);
}

function inputs() {
  return array(
    'username' => v::alnum()->noWhitespace()->length(1,15),
    'password' => v::notEmpty()->noWhitespace()->length(4,16)
  );
}

function validate($input) {
  $c = array();
  $v = inputs();

  foreach (array_keys($v) as $k) {
    if (isset($input[$k])) {
      if ($v[$k]->validate($input[$k])) {
        $c[$k] = $input[$k];
        slog('validasi field '.$k, 'true', 'Format benar', 'success');
      } else {
        //$c[$k] = '';
        slog('validasi field '.$k, 'false', 'Format salah', 'fail');
      }
    }
  }

  return $c;
}

$content = (isset($_POST['username']) && isset($_POST['password'])) ? validate(array('username' => $_POST['username'], 'password' => $_POST['password'])) : array();

function verifyTo($to) {
  global $db;
  $webhost = $db->getWebhost();
  if (base64_decode($to, true)) {
    $to = base64_encode(str_replace(array("\r", "\n"), '', base64_decode($to)));
    $decto = base64_decode($to);
    $host = parse_url($decto, PHP_URL_HOST);
    if (strcmp($host,$webhost)) {
      return $to;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

if (isset($_POST['q']) && ($_POST['q'] == 'login')) {
  $username = $content['username'];
  $password = $content['password'];
  session_start();
  session_destroy();

  if ((!isset($username) || empty($username)) || (!isset($password)) || empty($password)) {
    session_start();
    $_SESSION['message'] = array(
      'type' => 'danger',
      'content' => 'Username dan atau password salah.'
    );
//	var_dump($_SESSION['message']); die();
    //header('Location: login.php');
  }

  // kalau ada parameter to cek login tanpa session, dan redirect ke to.
  // dengan asumsi to adalah authorize confirmation
  // session akan dihancurkan disana, saat authorize confirmation yes.
  if (isset($_GET['to'])) {
    $check = $db->loginCheckNoSession($username, $password);
    $to = verifyTo($_GET['to']);
    if (($check == true) && (!is_array($check))) {
      session_start();
      if ($to !== false) {
        $_SESSION["username"] = $username;
        // CHANGES: :header manipulation: baris dibawah ini rawan perubahan external
        header('Location: '.str_replace(array("\r", "\n"), '', rawurldecode(base64_decode($to))));
      } else {
        header('Location: login.php');
      }
    } else {
      session_start();
      if ($to !== false) {
//        $to = base64_encode(str_replace(array("\r", "\n"), '', base64_decode($to)));
        $_SESSION['message'] = array(
          'type' => 'danger',
          'content' => 'Username dan atau password salah.'
        );
        header('Location: login.php?to='.$to);
      } else {
        header('Location: login.php');
      }
    }

    // kalau tidak ada parameter to, cek login dan simpan session dalam database.
    // dengan asumsi, ini adalah login untuk ke dashboard.
    // session akan dihancurkan ketika ada konfirmasi authorize yes dan ketika
    // logout melalui dashboard.
  } else {
    $check = $db->loginCheck($username, $password);
//    var_dump($check); //die();
    if (($check == true) && (!is_array($check))) {
      session_start();
      $_SESSION['last_activity'] = time();
      $_SESSION["username"] = $username;
      header('Location: dashboard.php');
    } else {
      $_SESSION['message'] = array(
        'type' => 'danger',
        'content' => 'Username dan atau password salah.'
      );
      header('Location: login.php');
    }
  }

// ini kalau proses logout
// hapus semua session di database dan file kalau ada.
} elseif (isset($_GET['q']) && ($_GET['q'] == 'logout')) {
  // CHANGES: :header manipulation: data tidak divalidasi di HTTP response, bisa menyebabkan open redirect dan hijack
  $to = (isset($_GET['to'])) ? '?to='. verifyTo($_GET['to']) : '';
  // echo $to;
  session_start();
  if (session_valid()) {
    if ($db->adaSession($_SESSION['username'])) {
      $query = sprintf("DELETE FROM ADM_MAGIC WHERE MAGIC_LOGIN = '%s' AND MAGIC_TIMEOUT IS NULL AND MAGIC_KICK IS NULL", $_SESSION['username']);
      // var_dump($query);
      if ($db->query($query, false)) {
        session_destroy();
        session_unset();
        // CHANGES: :header manipulation: issue di file user:121 diatas, bermuara disini.
        header('Location: login.php'.str_replace(array("\r", "\n"), '', $to));
      } else {
        header('Location: dashboard.php');
      }
    } else {
      session_destroy();
      session_unset();
      // CHANGES: :header manipulation: issue di file user.php:121 diatas juga bermuara disini.
      header('Location: login.php'.str_replace(array("\r", "\n"), '', $to));
    }
  } else {
    session_destroy();
    session_unset();
    // CHANGES: :header manipulation: issue di file user.php:121 diatas bermuara disini juga.
    header('Location: login.php'.str_replace(array("\r", "\n"), '', $to));
  }
} else {
  header('Location: login.php');
}
