<?php

/** Check session validity and activity
 *
 *  This function will check session validity and update last activity
 *
 *  @param  void
 *  @return boolean   true if valid, false if not.
 */
function session_valid() {
  session_start();
  // check if it expire
  if (isset($_SESSION['last_activity'])) {
    if ((time() - $_SESSION['last_activity']) > 600) { // set usia 10menit
      session_unset();    // unset variable $_SESSION
      session_destroy();  // destroy from sotrage
      return false;
    } else {
      $_SESSION['last_activity'] = time();
      return true;
    }
  } else {
    session_unset();
    session_destroy();
    return false;
  }
}
