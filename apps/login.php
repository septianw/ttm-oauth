<?php
  require_once __DIR__ . '/../vendor/autoload.php';
  use Respect\Validation\Validator as v;
  function validateUrl($input) {
    $input = base64_decode($input);
    $request = apache_request_headers();
    // ?response_type=code&client_id=75953c4337e72de3cdd112ac96e21bee&redirect_uri=http://www.example.com/&state=sdflkjhaksjd
    $result = v::call(
        'parse_url',
         v::arr()->key('scheme', v::startsWith('http'))
            ->key('host',   v::equals($request['Host']))
            ->key('path',   v::string()->noWhitespace()->contains('authorize.php'))
            ->key('query',  v::notEmpty())
    )->validate($input);
    
    return $result;
  }
  
  if (isset($_GET['to'])) {
    if (base64_decode($_GET['to'], true)) {
      $to = base64_encode(str_replace(array("\r", "\n"), '', base64_decode($_GET['to'])));
      if (validateUrl($to)) {
        $dest = '"user.php?to='.$to.'"';
      } else {
        $dest = '"user.php"';
      }
    }
  } else {
    $dest = '"user.php"';
  }
  // $dest = (isset($_GET['to'])) ? '"user.php?to='.$_GET['to'].'"' : '"user.php"';
  // $message = () ? : ;
  require_once 'session.php';
  if (session_valid()) {
    header('Location: dashboard.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Septian Wibisono">
    <link rel="shortcut icon" href="http://10.2.117.80:8800/web2sms/template/kumis/assets/ico/favicon.ico">

    <title>Teman telkomsel signin</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
      div.container {
        background-image: url("http://10.2.117.80:8800/web2sms/template/kumis/images/login_w2s.png");
        background-repeat: no-repeat;
        height: 500px;
        background-position: center;
      }
    </style>
  </head>

  <body>

    <?php
      if (isset($_SESSION['message'])) {
        switch ($_SESSION['message']['type']) {
          case 'success':
            echo '<div class="alert alert-success" role="alert">'.$_SESSION['message']['content'].'</div>';
            session_destroy();
            session_unset();
          break;
          case 'warning':
            echo '<div class="alert alert-warning" role="alert">'.$_SESSION['message']['content'].'</div>';
            session_destroy();
            session_unset();
          break;
          case 'info':
            echo '<div class="alert alert-info" role="alert">'.$_SESSION['message']['content'].'</div>';
            session_destroy();
            session_unset();
          break;
          case 'danger':
            echo '<div class="alert alert-danger" role="alert">'.$_SESSION['message']['content'].'</div>';
            session_destroy();
            session_unset();
          break;
        }
      }
    ?>

    <div class="container">
      <form class="form-signin" role="form" action=<?php printf('%s', $dest); ?> method="POST">
        <h2 class="form-signin-heading">Please sign in</h2>
        <input type="username" name="username" class="form-control" placeholder="Username" required autofocus>
        <input type="password" name="password" class="form-control" placeholder="Password" required>
        <input type="hidden" name="q" value="login">
        <!-- <label class="checkbox">
          <input type="checkbox" value="remember-me"> Remember me
        </label> -->
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
