if (typeof jQuery === 'undefined') { throw new Error('Bootstrap\'s JavaScript requires jQuery') }

$("[data-toggle='tooltip']").tooltip();

$( document ).ready(function() {
  var time = new Date().getTime();
  $(document.body).bind("mousemove keypress", function(e) {
    time = new Date().getTime();
  });

  function refresh() {
    if(new Date().getTime() - time >= 300000)
      // window.location.reload(true);
      window.location.replace(document.URL.substring( 0, document.URL.lastIndexOf("/") ) + '/user.php?q=logout');
    else
      setTimeout(refresh, 10000);
  }

  setTimeout(refresh, 10000);
})

function hapus(row) {
  var fulluri = $("#table-app")[0].baseURI;
  var baseuri = fulluri.substring(0, fulluri.lastIndexOf("/"));
  var initdata = {
    function: 'delete_row',
    CLIENT_ID: $("#table-app")[0].rows[row].cells[1].innerHTML,
    CLIENT_SECRET: $("#table-app")[0].rows[row].cells[2].innerHTML
  };

  console.log(baseuri);
  console.log(initdata);
  if (confirm('Anda akan menghapus aplikasi '+ $("#table-app")[0].rows[row].cells[0].innerHTML +' dari daftar anda?')) {
    $.ajax({
      type: "POST",
      url: baseuri+'/ajax.php',
      data: initdata,
      success: function(success, status) {
        // console.log(success);
        // console.log(status);
        if (status === 'success') {
          setTimeout(function() {
            window.location=fulluri;
          }, 1000);
          alert('Aplikasi sukses terhapus.');
        }
      }
    });
  }
  // console.log($("#table-app")[0].rows[row].cells[0].innerHTML);
}

function ubah(row) {
  if (confirm('Anda akan mengubah detail aplikasi ' + $("#table-app")[0].rows[row].cells[0].innerHTML + '.\n Anda hanya dapat mengubah nama aplikasi dan Redirect URL saja')) {
    var fulluri = $("#table-app")[0].baseURI;
    var baseuri = fulluri.substring(0, fulluri.lastIndexOf("/"));
    var initdata = {
      function: 'edit_row',
      CLIENT_ID: $("#table-app")[0].rows[row].cells[1].innerHTML,
      CLIENT_SECRET: $("#table-app")[0].rows[row].cells[2].innerHTML
    };

    // console.log(baseuri);
    // console.log(initdata);
    $.ajax({
      type: "POST",
      url: baseuri+'/ajax.php',
      data: initdata,
      success: function(success, status) {
        window.location=baseuri+'/addapp.php';
      }
    });
  }
}

function alertUbah() {
  var funct = $("#function").val();
  console.log(funct);
  if (funct == 'update_app') {
    alert('Detail aplikasi telah diubah.\nAnda akan dikembalikan ke dashboard dalam beberapa detik.');
  } else {
    alert('Detail aplikasi telah ditambahkan.\nAnda akan dikembalikan ke dashboard dalam beberapa detik.');
  }
//  setTimeout(function(){
//  }, 1000);
}

function saveapp() {
  $("")
}
