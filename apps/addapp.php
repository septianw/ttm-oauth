<?php
  require_once 'session.php';
  if (!session_valid()) { header('Location: login.php'); }
//   var_dump($_SESSION['datauser']);
   if (isset($_SESSION['datauser'])) {
     extract($_SESSION['datauser'][0]);
//     var_dump($CLIENT_ID);
  }
   if (isset($_POST['name']) && isset($_POST['uri'])) {
     $name = $_POST['name'];
     $uri = $_POST['uri'];
   }
?>

<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Septian Wibisono">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <!-- Font awesome CSS -->
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <title>Teman telkomsel</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script><style type="text/css"></style>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <style id="holderjs-style" type="text/css"></style></head>

  <body>

    <?php
      if (isset($_SESSION['message'])) {
        switch ($_SESSION['message']['type']) {
          case 'success':
            echo '<div class="alert alert-success" role="alert">'.$_SESSION['message']['content'].'</div>';
            unset($_SESSION['message']);
          break;
          case 'warning':
            echo '<div class="alert alert-warning" role="alert">'.$_SESSION['message']['content'].'</div>';
            unset($_SESSION['message']);
          break;
          case 'info':
            echo '<div class="alert alert-info" role="alert">'.$_SESSION['message']['content'].'</div>';
            unset($_SESSION['message']);
          break;
          case 'danger':
            echo '<div class="alert alert-danger" role="alert">'.$_SESSION['message']['content'].'</div>';
            unset($_SESSION['message']);
          break;
        }
      }
    ?>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="dashboard.php">Teman telkomsel</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="./dashboard.php"><i class="fa fa-th-list fa-lg"></i> List application</a></li>
            <li><a href="./user.php?q=logout"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
            <!-- <li><a href="dashboard.php">Profile</a></li>
            <li><a href="dashboard.php">Help</a></li> -->
          </ul>
          <!-- <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form> -->
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <!-- <div class="col-xs-6 col-md-4"></div> -->
        <div class="col-xs-6 col-md-4 main">

          <form name="addapp" method="post" class="form-add-app" role="form" action="ajax.php">
            <input type="text" name="name" class="form-control" placeholder="Application name" <?php printf('value="%s"', (isset($_SESSION['datauser'][0]['NAME'])) ? $_SESSION['datauser'][0]['NAME'] : ''); ?>>
            <input type="text" name="uri" class="form-control" placeholder="Redirect uri" <?php printf('value="%s"', (isset($_SESSION['datauser'][0]['REDIRECT_URI'])) ? $_SESSION['datauser'][0]['REDIRECT_URI'] : 'http://www.example.com/'); ?>>
            <input id="function" type="hidden" name="function" value="<?php echo (isset($NAME)) ? 'update_app' : 'add_app'; ?>">
            <button class="btn btn btn-primary btn-block" type="submit" onclick="alertUbah()">Save</button>
          </form>

        </div>
        <!-- <div class="col-xs-6 col-md-4"></div> -->
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <script src="js/dashboard.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
