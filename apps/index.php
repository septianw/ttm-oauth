<?php
require 'session.php';
if (!session_valid()) {
  header('Location: dashboard.php');
} else {
  header('Location: login.php');
}
