<?php
require_once(dirname(__FILE__).'/common/config.php');

define("KOSONG", "");

// error reporting (this is a demo, after all!)
// ini_set('display_errors',1);error_reporting(E_ALL);

require_once('oauth2-server-php/src/OAuth2/Autoloader.php');
require_once __DIR__ . '/vendor/autoload.php';
use Respect\Validation\Validator as v;

OAuth2\Autoloader::register();

class Gudang {

  protected $db;
  protected $config;

  function __construct($conf = null) {
    // require_once
    require_once(join(DIRECTORY_SEPARATOR, array(__DIR__, 'common', 'config.php')));
    // error_log(print_r($config));
    if (is_null($conf)) {
      $this->config = $this->process_config(get_config());
    } else {
      $this->config = $conf;
    }
    $this->db = new \PDO($this->config['dsn'], $this->config['username'], $this->config['password'], array());
    $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
  }

  private function encrypt_decrypt($action, $string) {
      $output = false;

      $encrypt_method = "AES-256-CBC";
      $secret_key = '9291a5025604812119b2a77bf9a5f0b4c0ffd83d411f19607a3d2be627518732c89c41a3b68c322346d90778e5236e2f706cea90f5db2c5f5c4cda7fabc433a7';
      $secret_iv = '2c7776c506e81dc976157cc142282a9baa600bc11a3cfffe680418ff2e447083f24810e9ce05d986529578e08419ce21874b2982e37734ec686a0ca4274e7027';

      // hash
      $key = hash('sha256', $secret_key);

      // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
      $iv = substr(hash('sha256', $secret_iv), 0, 16);

      if( $action == 'encrypt' ) {
          $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
          $output = base64_encode($output);
      }
      else if( $action == 'decrypt' ){
          $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
      }

      return $output;
  }

  function process_config($config) {
    $config['password'] = $this->encrypt_decrypt('decrypt', $config['password']);
    return $config;
  }

  function getWebhost() {
    return $this->config['webhost'];
  }

  public function getDb() {
    return $this->db;
  }

  public function dumpToLog($line, $variable, $mode = 'print') {
    $line = preg_replace("/[^0-9]/","",$line);
    switch ($mode) {
      case 'print':
        // CHANGES: :log forging: input kedalam log harus melalui validasi terlebih dahulu
        $variable = preg_replace("/[^A-Za-z0-9 ]/","",preg_replace("/[\r\n]/","_", print_r($variable, true)));
        error_log("from line " . $line . " of file " . __FILE__ . " " . $variable);
      break;
      case 'dump':
        ob_start();
//        var_dump($variable);
        $contents = ob_get_contents();
        ob_end_clean();
        $variable = preg_replace("/[^A-Za-z0-9 ]/","",preg_replace("/[\r\n]/","_", $contents));
        error_log("from line " . $line . " of file " . __FILE__ . " " . $variable);
      break;
      default:
        return 'mode should be print or dump';
    }
  }

  public function saveLog($payload) {
    $token = $payload['token'];
    // CHANGES: :sql inject: cek yang ini issue di file server:199 bersumber dari baris dibawah.
    $p_ip   = (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
    $p_ip = preg_replace("/[^0-9.]/","",$p_ip);
    // var_dump($token);
    $id = (is_null($token)) ? 'system' : $token['user_id'];
//    $record = array(
//      'LOG_ID' => '%d',
//      'LOG_MSISDN' => "'%s'",
//      'PAR_API_NAME' => "'%s'",
//      'LOG_EVENT' => "'%s'",
//      'LOG_REQ' => "'%s'",
//      'LOG_RETVAL' => "'%s'",
//      'LOG_DESC' => "'%s'",
//      'LOG_DATE' => 'SYSDATE'
//    );
//    $query = $this->susunqinsert('W2SMS_LOG_API', $record);
//    if (is_array($payload['request'])) {
//      if (array_key_exists('password', $payload['request'])) {
//        $payload['request']['password'] = KOSONG;
//      }
//    }
//    if (strlen($payload['retval']) > 950) {
//      $payload['retval'] = substr($payload['retval'], 0, 950).'... Content omitted ...';
//    }
//    $query = sprintf(
//      $query, time(), $id.'-'.$p_ip, pathinfo($payload['filename'], PATHINFO_FILENAME),
//      $payload['event'], json_encode($payload['request']), $payload['retval'],
//      $id.'-'.$token['access_token'].' '.$payload['desc']
//    );

    $record = array(
      'LOG_ID' => ':log_id',
      'LOG_MSISDN' => ":log_msisdn",
      'PAR_API_NAME' => ":par_api_name",
      'LOG_EVENT' => ":log_event",
      'LOG_REQ' => ":log_req",
      'LOG_RETVAL' => ":log_retval",
      'LOG_DESC' => ":log_desc",
      'LOG_DATE' => 'SYSDATE'
    );
    $query = $this->susunqBindInsert('W2SMS_LOG_API', $record);
    if (is_array($payload['request'])) {
      if (array_key_exists('password', $payload['request'])) {
        $payload['request']['password'] = KOSONG;
      }
    }
    if (strlen($payload['retval']) > 950) {
      $payload['retval'] = substr($payload['retval'], 0, 950).'... Content omitted ...';
    }
//    $query = sprintf(
//      $query,
//      time(),
//      $id.'-'.$p_ip,
//      pathinfo($payload['filename'], PATHINFO_FILENAME),
//      $payload['event'],
//      json_encode($payload['request']),
//      $payload['retval'],
//      $id.'-'.$token['access_token'].' '.$payload['desc']
//    );

    // perubahan ke bind param
    $bindparam = array(
      array( ':log_id', time(), PDO::PARAM_INT),
      array( ':log_msisdn', $id.'-'.$p_ip, PDO::PARAM_STR, 50), // CHANGES: SESUAIKAN LEBAR DATA DENGAN KONDISI DATABASE
      array( ':par_api_name', pathinfo($payload['filename'], PATHINFO_FILENAME), PDO::PARAM_STR, 50), // CHANGES: SESUAIKAN LEBAR DATA DENGAN KONDISI DATABASE
      array( ':log_event', $payload['event'], PDO::PARAM_STR, 50), // CHANGES: SESUAIKAN LEBAR DATA DENGAN KONDISI DATABASE
      array( ':log_req', json_encode($payload['request']), PDO::PARAM_STR, 1000), // CHANGES: SESUAIKAN LEBAR DATA DENGAN KONDISI DATABASE
      array( ':log_retval', $payload['retval'], PDO::PARAM_STR, 1000), // CHANGES: SESUAIKAN LEBAR DATA DENGAN KONDISI DATABASE
      array( ':log_desc', $id.'-'.$token['access_token'].' '.$payload['desc'], PDO::PARAM_STR, 700) // CHANGES: SESUAIKAN LEBAR DATA DENGAN KONDISI DATABASE
    );

    // echo $query;

//  contoh bind param

//  $query = "SELECT * FROM OAUTH_CLIENTS WHERE CLIENT_ID = :dataid AND CLIENT_SECRET = :datasecret AND USER_ID = :userid";
//
//  $bindparam = array(
//    array( ':dataid', $data['CLIENT_ID'], PDO::PARAM_STR, 80),
//    array( ':datasecret', $data['CLIENT_SECRET'], PDO::PARAM_STR, 80),
//    array( ':userid', $_SESSION['username'], PDO::PARAM_STR, 80)
//  );
//
//  // var_dump($query);
//  if ($data = $db->query($query,true,$bindparam)) {
//    // var_dump($data);
//    $_SESSION['datauser'] = $data;
//  }


    $this->query($query, false, $bindparam);
  }

  public function toLowerKey ($input) {
    $output = array();
    if (is_array($input)) {
      foreach ($input as $key => $value) {
        $output[strtolower($key)] = $value;
      }
    }
    return $output;
  }

  public function getrand() {
    $randres = fopen('/dev/urandom', "r");
    $read = fread($randres, 2048);
    fclose($randres);
    return hash("ripemd128", hash("sha512", $read));
  }

  protected function toUpperKey ($input) {
    $output = array();
    if (is_array($input)) {
      foreach ($input as $key => $value) {
        $output[strtoupper($key)] = $value;
      }
    }
    return $output;
  }

  protected function convertDate($oradate) {
    $date = DateTime::createFromFormat('d-M-y h.i.s A', $oradate);
    $this->dumpToLog( __LINE__, $date);
    return $date->format('U');
  }

  public function rewriteNumber($start, $data) {
    $start = (int)$start;
    foreach ($data as $row => $array) {
      $data[$row]['NUMBER'] = $start;
      $start++;
    }
    return $data;
  }

  public function getUserByToken($token) {
    $query = sprintf("
    SELECT OC.USER_ID USER_ID
      FROM OAUTH_CLIENTS OC
      JOIN OAUTH_ACCESS_TOKENS OAT
        ON OAT.CLIENT_ID = OC.CLIENT_ID
     WHERE OAT.ACCESS_TOKEN = '%s'
    ", $token);
    $result = $this->query($query);

    return $result[0]['USER_ID'];
  }

  public static function susunqinsert($table, $field) {
    $q = "INSERT INTO ".$table." (";
    $c = 1;
    // insert key
    foreach($field as $key => $value) {
      if ($c == count($field)) {
        $q .= $key.') ';
      } else {
        $q .= $key.', ';
      }
      $c++;
    }

    $q .= "VALUES (";
    $c = 1;
    foreach($field as $key => $value) {
      if ($c == count($field)) {
        $q .= $value.") ";
      } else {
        $q .= $value.", ";
      }
      $c++;
    }

    return $q;
  }

  public static function susunqBindInsert($table, $field) {
    $q = "INSERT INTO ".$table." (";
    $c = 1;
    // insert key
    foreach($field as $key => $value) {
      if ($c == count($field)) {
        $q .= $key.') ';
      } else {
        $q .= $key.', ';
      }
      $c++;
    }

    $q .= "VALUES (";
    $c = 1;
    foreach($field as $key => $value) {
      if ($c == count($field)) {
        $q .= $value.") ";
      } else {
        $q .= $value.", ";
      }
      $c++;
    }

    return $q;
  }

  /**
   * query
   *
   * execute the SQL syntax
   *
   * @param  string  $q           String query
   * @param  boolean [$fetch     = true]     boolean fetch, if true the query will be fetched
   * @param  array   [$bindparam = null] array of parameter want to bind or null if not used
   * @return mixed   mixed return. if fetched return array, if not fetched return boolean.
   */
  public function query($q, $fetch = true, $bindparam = null) {
    // CHANGES: :sql inject: butuh input validation, untuk lebih aman sebaiknya pakai bind param.
    $exe = $this->db->prepare($q);
    if (!is_null($bindparam)) {
      // $exe->bindParam(':msisdn', $msisdn, PDO::PARAM_STR, 20);
      foreach ($bindparam as $k => $param) {
        if (isset($param[3])) {
          $exe->bindParam($param[0], $param[1], $param[2], $param[3]);
        } else {
          $exe->bindParam($param[0], $param[1], $param[2]);
        }
      }
    }
    // $this->dumpToLog(__LINE__,  $exe);
    if ($fetch) {
      $exe->execute();
      // CHANGES: :sql inject: sebelum dieksekusi, jalankan pengujian validasi dulu.
      return $exe->fetchAll(PDO::FETCH_ASSOC);
    } else {
      return $exe->execute();
    }
  }

  public function queryLimit($q, $start, $end, $fetch = true, $order = null, $bindparam = null) {
    // $end = (($end - $start) <= 9) ? $end : $start + 9;

    $this->dumpToLog(__LINE__, $end);
    if (is_null($order)) {
      $query = sprintf("SELECT * FROM ( SELECT ROWNUM \"NUMBER\", MAINQ.* FROM ( %s ) MAINQ WHERE ROWNUM <= :akhir ) WHERE \"NUMBER\" >= :mulai ", $q);
      $limobj = array(
        array(':akhir', $end, PDO::PARAM_INT),
        array(':mulai', $start, PDO::PARAM_INT)
      );
    } else {
      $field = array_keys($order);
      $sort = array_values($order);
      $query = sprintf("SELECT * FROM ( SELECT ROWNUM \"NUMBER\", MAINQ.* FROM ( %s ) MAINQ WHERE ROWNUM <= :akhir ) WHERE \"NUMBER\" >= :mulai ORDER BY %s %s",
      $q, $field[0], $sort[0]);
      $limobj = array(
        array(':akhir', $end, PDO::PARAM_INT),
        array(':mulai', $start, PDO::PARAM_INT),
      );
    }
    // var_dump($query);
    // $exe->bindParam(':msisdn', $msisdn, PDO::PARAM_STR, 20);
    // CHANGES: :sql inject: butuh input validasi, cek $query untuk malicious code
    $exe = $this->db->prepare($query);

    // bind parameter disini
    if (is_null($bindparam)) {
      $params = $limobj;
    } else {
      $params = array_merge($bindparam, $limobj);
    }

    foreach ($params as $k => $param) {
      if (isset($param[3])) {
        $exe->bindParam($param[0], $param[1], $param[2], $param[3]);
      } else {
        $exe->bindParam($param[0], $param[1], $param[2]);
      }
    }

    $this->dumpToLog(__LINE__,  $query);
    $exe->execute();
    if ($fetch) {
      $exe->execute();
      return $this->rewriteNumber($start, $exe->fetchAll(PDO::FETCH_ASSOC));
    } else {
      $exe->execute();
    }
  }

  public function check_status($type,$status){
    $result ="";
    if($type==1){
      switch ($status) {
        case 1:
          $result = "Member Active";
        break;
        case 2:
          $result = "Will Confirm";
        break;
        case 3:
          $result = "Sent Not Reply";
        break;
        case 4:
          $result = "Member Decline";
        break;
        case 5:
          $result = "Member Quit";
        break;
        case 6:
          $result = "MSISDN Expired";
        break;
      }
    }elseif($type==2){
      switch ($status) {
        case 1:
          $result = "Group Active";
        break;
        case 2:
          $result = "Will Confirm";
        break;
        case 3:
          $result = "Sent Not Reply";
        break;
        case 4:
          $result = "Group Decline";
        break;
        case 5:
          $result = "Group Quit";
        break;
        case 6:
          $result = "Group Expired";
        break;
      }
    }
    return $result;
  }

  public function noPdo () {
    $dsn = sprintf('(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=%s)(PORT=%s))(CONNECT_DATA=(SID=%s)))',
                    $this->config['param']['host'], $this->config['param']['port'], $this->config['param']['sid']);
//	var_dump($this->config);// die();
//	$this->dumpToLog(__LINE__, json_encode($dsn));  // left here for debugging purpose
//	$this->dumpToLog(__LINE__, json_encode($this->config)); // left here for debugging purpose
    $dbid = oci_connect($this->config['username'], $this->config['password'],$dsn);

    if (!$dbid) {
      $e = oci_error();
      trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }

    return $dbid;
  }

  protected function hapusSession($username) {
    $query = sprintf("DELETE FROM ADM_MAGIC WHERE MAGIC_TIMEOUT IS NULL AND MAGIC_KICK IS NULL AND MAGIC_LOGIN = '%s'", $username);
    return $this->query($query, false);
  }

  public function loginCheckNoSession($username, $password) {
    if ($this->loginCheck($username, $password) === true) {
      return $this->hapusSession($username);
    }
  }

  public function loginCheck($username, $password) {
    $p_user = $username;
    $p_pass = $password;
    $p_ip   = (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
    $p_host = 'remote.api';//(!isset($_SERVER['REMOTE_HOST'])) ? $_SERVER['REMOTE_HOST'] : 'remote.api';

    $dbid = $this->noPdo();

		$stid = oci_parse($dbid, 'begin :r := main_authentication(:p_user,:p_pass,:p_ip,:p_host); end;');

		oci_bind_by_name($stid, ':p_user', $p_user);
		oci_bind_by_name($stid, ':p_pass', $p_pass);
		oci_bind_by_name($stid, ':p_ip', $p_ip);
		oci_bind_by_name($stid, ':p_host', $p_host);
		oci_bind_by_name($stid, ':r', $r, 4000);
		oci_execute($stid);
		oci_free_statement($stid);
    $this->dumpToLog(__LINE__, $r);

    switch ($r) {
      case 10:
      case  9:
        $this->dumpToLog(__LINE__, 'false');
        return array('success' => 'false', 'description' => 'account has been locked (SCR-0006).');
      break;
      case  8:
        $this->dumpToLog(__LINE__, 'false');
        return array('success' => 'false', 'description' => 'Invalid Username (SCR-0008).');
      break;
      case 12:
        $this->dumpToLog(__LINE__, 'false');
        return array('success' => 'false', 'description' => 'account was already In-Active (SCR-0098).');
      break;
      case  2:
        $this->dumpToLog(__LINE__, 'false');
        return array('success' => 'false', 'description' => 'User still running (SCR-0002).');
      break;
      case 13:
        $this->dumpToLog(__LINE__, 'true');
        return true;
        // return $this->hapusSession($username);
      break;
    }
  }

  public function adaSession($username) {
    $query = sprintf("SELECT MAGIC_LOGIN FROM ADM_MAGIC WHERE MAGIC_TIMEOUT IS NULL AND MAGIC_KICK IS NULL AND MAGIC_LOGIN = '%s'", $username);
    $result = $this->query($query);
    if ((isset($result[0]['MAGIC_LOGIN'])) && ($result[0]['MAGIC_LOGIN'] == $username)) {
      return true;
    } else {
      return false;
    }
  }
}

class Util {
  // KeepSmsLog($smshid, $val['MSISDN'], 'FAIL', 'Member not active.', $message)
  public static function KeepSmsLog ($smshid, $msisdn, $status, $reason = '', $msg = '') {
    $dbs = new Gudang();
    // INSERT INTO W2SMS_SEND_ERR (
    //   SEND_ID, SMSHID, MSISDN, STATUS, ERR_DESC, MSGTXT
    // ) VALUES (
    //   SEQ_W2SMS_SEND.NEXTVAL, 1111, '8475769489', 'BAD', 'FINE', 'I''M FINE AND BAD AT THE SAME TIME'
    // );
    if ($status != 'SUCCESS') {
      $query = Gudang::susunqinsert('W2SMS_SEND_ERR', array(
        'SEND_ID' => 'SEQ_W2SMS_SEND.NEXTVAL',
        'SMSHID' => $smshid,
        'MSISDN' => "'$msisdn'",
        'STATUS' => "'$status'",
        'ERR_DESC' => "'$reason'",
        'MSGTXT' => "'$msg'"
      ));
      return $dbs->query($query, false);
    }
//    var_dump($query); die();
  }
  
  /**
   * Get GID from groupname
   * 
   * @param String $group Name of group.
   * @return  int|boolean
   */
  public static function gidDariNama($group) {
    $dbs = new Gudang();
    $que = sprintf("SELECT GID FROM W2SMS_GROUP WHERE GNAME = '%s'",strtoupper($group));
    $gid = $dbs->query($que);
//    var_dump(isset($gid[0]['GID'])); die();
    if (isset($gid[0]['GID'])) {
//      var_dump((int)$gid[0]['GID']); die();
      return (int)$gid[0]['GID'];
    } else {
      return false;
    }
  }
  
//  public static function testBacktrace() {
//    var_dump();
//  }
  
  public static function validateOutput($output) {
    $fout = array();
    $btrace = debug_backtrace();
    $string = v::alnum(".,?'\"-()!@:\n");
    $integer = v::int();
    $date = v::date();
    
    $payload = array(
      'token' => null,
      'filename' => $btrace[0]['file'],
      'request' => $output,
      'retval' => '',
      'desc' => '',
      'event' => 'Validate output fail'
    );

//function slog($request, $retval, $desc, $event) {
//  global $db;
//  global $token;
//  $payload = array(
//    'token' => $token,
//    'filename' => __FILE__,
//    'request' => $request,
//    'retval' => $retval,
//    'desc' => $desc,
//    'event' => $event
//  );
//  $db->saveLog($payload);
//}

    $gudang = new Gudang();
//    $gudang->saveLog()
//    var_dump($output); die();
    foreach($output as $key => $value) {
      if (is_array($value)) {
        $fout[$key] = Util::validateOutput($value);
      }
      
      if (is_string($value)) {
        if ($key == 'timestamp') {
          if ($date->validate($value)) {
            $fout[$key] = $value;
          } else {
            $payload['retval'] = json_encode($fout);
            $payload['desc'] = 'Format output invalid';
            $gudang->saveLog($payload);
          }
        } else {
          if ($string->validate($value)) {
            $fout[$key] = $value;
          } else {
            $payload['retval'] = json_encode($fout);
            $payload['desc'] = 'Format output invalid';
            $gudang->saveLog($payload);
          }
        }
      }
      
      if (is_int($value)) {
        if ($integer->validate($value)) {
          $fout[$key] = $value;
        } else {
          $payload['retval'] = json_encode($fout);
          $payload['desc'] = 'Format output invalid';
          $gudang->saveLog($payload);
        }
      }
      
    }
    
    return $fout;
  }
}

$db = new Gudang();

$storage = new OAuth2\Storage\Pdo($db->process_config(get_config()));

// configure your available scopes

$memory = new OAuth2\Storage\Memory(array(
  'default_scope' => 'user',
  'supported_scopes' => array(
    'user',
    'apps'
  )
));

// var_dump($storage);

// Pass a storage object or array of storage objects to the OAuth2 server class
$server = new OAuth2\Server($storage);

$server->setScopeUtil(new OAuth2\Scope($memory));

// Add the "Client Credentials" grant type (it is the simplest of the grant types)
$server->addGrantType(new OAuth2\GrantType\ClientCredentials($storage));

// Add the "Authorization Code" grant type (this is where the oauth magic happens)
$server->addGrantType(new OAuth2\GrantType\AuthorizationCode($storage));

$server->addGrantType(new OAuth2\GrantType\RefreshToken($storage,array(
    'always_issue_new_refresh_token' => true,
    'refresh_token_lifetime'         => 1209600,
)));
