<?php
// include our OAuth2 Server object
require_once __DIR__.'/server.php';
$db = new Gudang();
require_once __DIR__ . '/vendor/autoload.php';
use Respect\Validation\Validator as v;

$return['timestamp'] = date("Y-m-d H:i:s");
$return['id'] = time();
$return['status'] = null;

$request = OAuth2\Request::createFromGlobals();
$response = new OAuth2\Response();
$content = $request->request;

$token = $server->getAccessTokenData(OAuth2\Request::createFromGlobals());

function slog($request, $retval, $desc, $event) {
  global $db;
  global $token;
  $payload = array(
    'token' => $token,
    'filename' => __FILE__,
    'request' => $request,
    'retval' => $retval,
    'desc' => $desc,
    'event' => $event
  );
  $db->saveLog($payload);
}

// Handle a request for an OAuth2.0 Access Token and send the response to the client
$scopeRequired = 'user';
if (!$server->verifyResourceRequest($request, $response, $scopeRequired)) {
  slog($content, json_encode($response->getParameters()), 'token authorize fail', 'fail');
  // echo "satu";
  $server->getResponse()->send();
  exit();
}

function get_userinfo() {
  global $token;
  global $db;
  $query = sprintf("SELECT * FROM W2SSMS_SUB_COMMUNITY WHERE USERLOGIN = '%s'", $token['user_id']);
  $outq = $db->query($query);
  
  return $outq[0];
}

function returnFail($code, $reason = '', $request = null) {
  global $return;
  global $db;
  global $token;
  // var_dump($return);

  switch ($code) {
    case 'OK':
      $return['status'] = $code;
      header('Content-Type: application/json');
      slog($request, json_encode($return), $reason, $code);
      $return = Util::validateOutput($return);
      echo json_encode($return);
    break;
    case 'AUTH_FAILED':
      $return['status'] = $code;
      $return['reason'] = $reason;
      header('Content-Type: application/json');
      slog($request, json_encode($return), $reason, $code);
      $return = Util::validateOutput($return);
      echo json_encode($return);
    break;
    case 'GROUP_FAILED':
      $return['status'] = $code;
      header('Content-Type: application/json');
      slog($request, json_encode($return), $reason, $code);
      $return = Util::validateOutput($return);
      echo json_encode($return);
    break;
    case 'GROUP_EXIST':
      $return['status'] = $code;
      header('Content-Type: application/json');
      slog($request, json_encode($return), $reason, $code);
      $return = Util::validateOutput($return);
      echo json_encode($return);
    break;
    case 'KEYWORD_EXIST':
      $return['status'] = $code;
      header('Content-Type: application/json');
      slog($request, json_encode($return), $reason, $code);
      $return = Util::validateOutput($return);
      echo json_encode($return);
    break;
  }
}

function adaGroup($db, $groupname) {
  $query = $db->query(sprintf("SELECT GNAME FROM W2SMS_GROUP WHERE GNAME = '%s'", strtoupper($groupname)));
  // var_dump($query);
  if (isset($query[0]['GNAME'])) {
    return true;
  } else {
    return false;
  }
}

function adaKeyword($db, $keyword) {
  $query = $db->query(sprintf("SELECT REG_SMS FROM W2SMS_GROUP WHERE REG_SMS = '%s'", strtoupper($keyword)));
  // var_dump($query);
  if (isset($query[0]['REG_SMS'])) {
    return true;
  } else {
    return false;
  }
}

// susun query

function susunQuery($content) {
  global $token;
  global $db;

  $user = get_userinfo();
  $field = array(
    'GID' => 'SEQ_W2SMS_GROUP_ID.NEXTVAL + 1',
    'GNAME' => "'%s'",
    'REG_SMS' => "'%s'",
    'G_MID' => $user['SUB_ID'],
    'CREATED_BY' => "'".$token['user_id']."'",
    'CREATED_DATE' => 'SYSDATE'
  );

  if (isset($content['desc'])) {
    $field['DESCRIPTION'] = "'%s'";
  }
  if (isset($content['limit'])) {
    $field['MAX_MEMBER'] = '%d';
  }

  $rawq = $db->susunqinsert('W2SMS_GROUP', $field);

  // var_dump($rawq);

  if (isset($content['desc']) && (isset($content['limit']))) {
    $query = sprintf($rawq,
      strtoupper($content['group_name']),
      strtoupper($content['format_sms']),
      $content['desc'],
      (int) $content['limit']
    );
  } elseif (isset($content['desc'])) {
    $query = sprintf($rawq,
      strtoupper($content['group_name']),
      strtoupper($content['format_sms']),
      $content['desc']);
  } elseif (isset($content['limit'])) {
    $query = sprintf($rawq,
      strtoupper($content['group_name']),
      strtoupper($content['format_sms']),
      (int) $content['limit']);
  } else {
    $query = sprintf($rawq,
      strtoupper($content['group_name']),
      strtoupper($content['format_sms']));
  }

  return $query;
}

function addGroup($content) {
  global $db;
  if (isset($content['group_name']) && isset($content['format_sms'])) {
    if (!adaGroup($db, $content['group_name'])) {
      if (!adaKeyword($db, $content['format_sms'])) {
        // echo 'insert bro';
        $query = susunQuery($content);
         var_dump($query);
        if ($db->query($query, false)) {
          returnFail('OK','',$content);
        }
      } else {
        returnFail('KEYWORD_EXIST','',$content);
      }
    } else {
      returnFail('GROUP_EXIST','',$content);
    }
  } else {
    returnFail('GROUP_FAILED','',$content);
  }
}

function inputs() {
  return array(
    'group_name' => v::alnum()->noWhitespace()->length(1,40),
    'desc' => v::alnum()->length(1,400),
    'limit' => v::int(),
    'format_sms' => v::alnum()->noWhitespace()->length(1,20),
  );
}

function validate($input) {
  $c = array();
  $v = inputs();

  foreach (array_keys($v) as $k) {
    if (isset($input[$k])) {
      if ($v[$k]->validate($input[$k])) {
        $c[$k] = $input[$k];
        slog('validasi field '.$k, 'true', 'Format benar', 'success');
      } else {
        //$c[$k] = '';
        slog('validasi field '.$k, 'false', 'Format salah', 'fail');
      }
    }
  }

  return $c;
}

$content = validate($content);

// cek login dulu baru insert
// NOTE: 14 march 2017: no more username check
addGroup($content);

