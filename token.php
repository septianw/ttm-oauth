<?php
// include our OAuth2 Server object
require_once __DIR__.'/server.php';
$db = new Gudang();
require_once __DIR__ . '/vendor/autoload.php';
use Respect\Validation\Validator as v;

// Handle a request for an OAuth2.0 Access Token and send the response to the client
// var_dump(OAuth2\Request::createFromGlobals());
$server->handleTokenRequest(OAuth2\Request::createFromGlobals())->send();
