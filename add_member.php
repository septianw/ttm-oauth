<?php
// include our OAuth2 Server object
require_once __DIR__.'/server.php';
$db = new Gudang();
require_once __DIR__ . '/vendor/autoload.php';
use Respect\Validation\Validator as v;

$return['timestamp'] = date("Y-m-d H:i:s");
$return['id'] = time();
$return['status'] = null;


$request = OAuth2\Request::createFromGlobals();
$response = new OAuth2\Response();
$content = $request->request;

$token = $server->getAccessTokenData(OAuth2\Request::createFromGlobals());

function slog($request, $retval, $desc, $event) {
  global $db;
  global $token;
  $payload = array(
    'token' => $token,
    'filename' => __FILE__,
    'request' => $request,
    'retval' => $retval,
    'desc' => $desc,
    'event' => $event
  );
  $db->saveLog($payload);
}

// Handle a request for an OAuth2.0 Access Token and send the response to the client
$scopeRequired = 'user';
if (!$server->verifyResourceRequest($request, $response, $scopeRequired)) {
  slog($content, json_encode($response->getParameters()), 'token authorize fail', 'fail');
  // echo "satu";
    $server->getResponse()->send();
    exit();
}

function url_get_contents($Url) {
    if (!function_exists('curl_init')){
        die('CURL is not installed!');
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $Url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}

function returnFail($code, $reason = '', $request = null) {
  global $return;
  global $db;
  global $token;
  // var_dump($return);

  switch ($code) {
    case 'OK':
      $return['status'] = $code;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
    case 'AUTH_FAILED':
      $return['status'] = $code;
      $return['reason'] = $reason;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
    case 'MSISDN_FAILED':
      $return['status'] = $code;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
    case 'GROUP_FAILED':
      $return['status'] = $code;
      $return['reason'] = $reason;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
    case 'MSISDN_EXIST':
      $return['status'] = $code;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
  }
}



function msisdnAda($db, $msisdn) {
  $query = $db->query(sprintf("
    SELECT MSISDN FROM W2SMS_CMR_MEMBER WHERE MSISDN = '%s'",
    $msisdn));
  
  return isset($query[0]['MSISDN']);
}

function msisdnGakValid($msisdn) {
  // buka baris² dibawah ini ketika siap

  if (substr($msisdn, 0, 1) == '8') {
    $msisdn = '62'.$msisdn;
  } elseif (substr($msisdn, 0, 2) == '08') {
    $msisdn = '62'.substr($msisdn, 1);
  }

  $valid = file_get_contents('http://10.1.89.201:8081/Cmessg/Subscription?userid=w2smscom&msisdn='.$msisdn);
  if (strlen($valid) > 0 && strlen(trim($valid)) == 0) {
    // echo $valid;
    return true;
    // gak valid
  } else {
    // echo $valid;
    return false;
    // valid
  }

  // return false;
}

function formatMsisdn($msisdn) {
  if (substr($msisdn, 0, 1) == '8') {
    return $msisdn;
  } elseif (substr($msisdn, 0, 2) == '08') {
    return substr($msisdn, 1);
  } elseif (substr($msisdn, 0, 3) == '628') {
    return substr($msisdn, 2);
  }
}

function gidDariNama($db, $group) {
  $gid = $db->query(sprintf("SELECT GID FROM W2SMS_GROUP WHERE GNAME LIKE '%%%s%%'",strtoupper($group)));
  $db->dumpToLog(__LINE__, $gid);
  if (isset($gid[0]['GID'])) {
    return $gid[0]['GID'];
  } else {
    return false;
  }
}

function insertMember($db, $fname, $lname, $msisdn) {
  global $token;
  $query = sprintf("
    SELECT USERLOGIN, SUB_ID, COMMID FROM W2SSMS_SUB_COMMUNITY WHERE USERLOGIN = '%s'
  ", $token['user_id']
  );
  $user = $db->query($query);
  
  $query = sprintf('SELECT * FROM CMR_SUB_COMMUNITY@LN_CMS WHERE SUB_ID = %s  and rownum = 1', $user[0]['SUB_ID']);
  $commname = $db->query($query);
//  $	= $this->solusi247->get_data_nonObject('CMR_SUB_COMMUNITY@LN_CMS','COMMID',"'".$commid."'",'SUB_NAME');
  
  if (isset($commname[0])) {
    $cid = $commname[0]['CID'];  // ATTN This line need some attention, no guarantee if field CID was there
    $url = url_get_contents(sprintf("http://10.2.117.80:8080/commetapi/insert_member?cid='%s'&sub_id='%s'&msisdn='%s'", $cid, $user[0]['SUB_ID'], $msisdn));
    
    $query = sprintf('SELECT MSG_TEXT FROM W2SMS_PARAMETER_MSG_NON where keyword_id = 1003');
    $kw = $db->query($query);
    $kw = $kw[0]['MSG_TEXT'];
    $find_commname = str_replace("~community_name",$commname[0]['SUB_NAME'],$kw);
    $find_text  = str_replace("~code_comm",$commname[0]['COMMID'],$find_commname);
    
    $queuesms = sprintf("INSERT INTO WF_QUEUE_SENDSMS(MSGID,CORRID,ENQ_TIME,MSISDN,MSGTXT,STATUS)
                        VALUES (SEQ_SMS2SEND.NEXTVAL,'SYSTEM',sysdate,'%s','%s','READY')", $msisdn, $find_text);

    $query = sprintf("
      INSERT INTO W2SMS_CMR_MEMBER (MID, FNAME, LNAME, MSISDN, CREATE_BY, SUB_ID, COMMID)
      VALUES (SEQ_CMR_MEMBER_ID.NEXTVAL, '%s', '%s', '%s', '%s', '%s', '%s')
    ", $fname, $lname, formatMsisdn($msisdn), $token['user_id'], $user[0]['SUB_ID'], $user[0]['COMMID']);
    
    return $db->query($queuesms, false) && $db->query($query, false);
  } else {
    return false;
  }
}

function isPenuh($groupName) { // bool
  $return = false;
  global $db;
  
  $gid = gidDariNama($db, strtoupper($groupName));
  $query = sprintf("SELECT COUNT(*) CAP FROM W2SMS_GROUP_MEMBER A, W2SMS_GROUP B WHERE A.GID = B.GID AND A.GID = %d", $gid);
  $qout = $db->query($query);
  $cap = $qout[0]['CAP'];
  $query = sprintf("SELECT MAX_MEMBER FROM W2SMS_GROUP WHERE GID = %d", $gid);
  $qout = $db->query($query);
  $max = $qout[0]['MAX_MEMBER'];
  
  if ((($cap == $max) || ($cap > $max)) && (!is_null($qout[0]['MAX_MEMBER']))) {
    slog($content, 'group full', sprintf('Group capacity is %d, and number of member is %d.', $max, $cap), 'Try to push more member to group.');
    $return = true;
  }
  
  return $return;
}

function addMember($content) {
  global $db;
  if (isset($content['msisdn'])) {
    if (msisdnGakValid($content['msisdn']) || !isset($content['msisdn']) ) {
      returnFail('MSISDN_FAILED', '', $content);
    } elseif (msisdnAda($db, formatMsisdn($content['msisdn']))) {
      returnFail('MSISDN_EXIST', '', $content);
    } elseif (!isset($content['group_name'])) {

      $firstname = (isset($content['first_name'])) ? $content['first_name'] : '';
      $lastname = (isset($content['last_name'])) ? $content['last_name'] : '';

      if (insertMember($db, $firstname, $lastname, $content['msisdn'])) {
        // echo 'OK';
        returnFail('OK','',$content);
      } else {
        returnFail('GROUP_FAIL', 'User does not have any groups');
      }
    } elseif (isPenuh($content['group_name'])) {
      returnFail('GROUP_FAILED', 'This group is full', $content);
    } else {
      if ($gid = gidDariNama($db, strtoupper($content['group_name']))) {

        $firstname = (isset($content['first_name'])) ? $content['first_name'] : '';
        $lastname = (isset($content['last_name'])) ? $content['last_name'] : '';

        if (insertMember($db, $firstname, $lastname, $content['msisdn'])) {
          $mid = $db->query("SELECT SEQ_CMR_MEMBER_ID.CURRVAL MID FROM DUAL");
          $query = sprintf("
            INSERT INTO W2SMS_GROUP_MEMBER (GMID, MID, GID, CREATE_DATE, MSISDN)
            VALUES (SEQ_W2SMS_GROUP_MEMBER_ID.NEXTVAL, %d, %d, SYSDATE, '%s')",
            (int) $mid[0]['MID'],
            (int) $gid,
            formatMsisdn($content['msisdn']));

          if ($db->query($query, false)) { // ATTN no else here, is it fine?
            // berhasil
            returnFail('OK','',$content);
          } else {
            returnFail('GROUP_FAILED','',$content);
          }
        } else {
          returnFail('GROUP_FAILED', 'User does not have any groups.');
        }
      } else {
        returnFail('GROUP_FAILED','',$content);
      }
    }
  } else {
    returnFail('MSISDN_FAILED', '', $content);
  }
}

function inputs() {
  return array(
    'msisdn' => v::notEmpty()->numeric()->positive()->length(9,20),
    'first_name' => v::alpha()->length(1,60),
    'last_name' => v::alpha()->length(1,20),
    'group_name' => v::alnum()->noWhitespace()->length(1,40)
  );
}

function validate($input) {
  $c = array();
  $v = inputs();

  foreach (array_keys($v) as $k) {
    if (isset($input[$k])) {
      if ($v[$k]->validate($input[$k])) {
        $c[$k] = $input[$k];
        slog('validasi field '.$k, 'true', 'Format benar', 'success');
      } else {
        //$c[$k] = '';
        slog('validasi field '.$k, 'false', 'Format salah', 'fail');
      }
    }
  }

  return $c;
}

$content = validate($content);

// cek login dulu baru insert
// NOTE: 14 march 2017: no more username check
addMember($content);
