<?php
// include our OAuth2 Server object
require_once __DIR__.'/server.php';
$db = new Gudang();
require_once __DIR__ . '/vendor/autoload.php';
use Respect\Validation\Validator as v;


$request = OAuth2\Request::createFromGlobals();
$response = new OAuth2\Response();
$content = $request->request;

$token = $server->getAccessTokenData($request);

function slog($request, $retval, $desc, $event) {
  global $db;
  global $token;
  $payload = array(
    'token' => $token,
    'filename' => __FILE__,
    'request' => $request,
    'retval' => $retval,
    'desc' => $desc,
    'event' => $event
  );
  $db->saveLog($payload);
}

// https://api.example.com/resource-requiring-postonwall-scope
$scopeRequired = 'apps'; // this resource requires "postonwall" scope
if (!$server->verifyResourceRequest($request, $response, $scopeRequired)) {
  slog($content, json_encode($response->getParameters()), 'token authorize fail', 'fail');
  // if the scope required is different from what the token allows, this will send a "401 insufficient_scope" error
  // echo 'lima';
  $response->send();
  exit();
}

$return['timestamp'] = date("Y-m-d H:i:s");
$return['id'] = time();
$return['status'] = 'AUTH_FAILED';


function returnFail($code, $reason = '', $request = null) {
  global $return;
  global $db;
  global $token;
  // var_dump($return);
  switch ($code) {
    case 'OK':
      $return['status'] = $code;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
    case 'USER_NOTFOUND':
      $return['status'] = $code;
      $return['reason'] = $reason;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
    case 'PARAMETER_INCOMPLETE':
      $return['status'] = $code;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
  }
}

function susunqinsert($table, $field) {
  $q = "INSERT INTO ".$table." (";
  $c = 1;
  // insert key
  foreach($field as $key => $value) {
    if ($c == count($field)) {
      $q .= $key.') ';
    } else {
      $q .= $key.', ';
    }
    $c++;
  }

  $q .= "VALUES (";
  $c = 1;
  foreach($field as $key => $value) {
    if ($c == count($field)) {
      $q .= "'".$value."') ";
    } else {
      $q .= "'".$value."', ";
    }
    $c++;
  }

  return $q;
}

function susunqselect($table, $field) {
  $q = sprintf("SELECT * FROM %s WHERE USER_ID = '%s' AND REDIRECT_URI = '%s'",
        $table, $field['USER_ID'], $field['REDIRECT_URI']);

  return $q;
}

// CLIENT_ID
// CLIENT_SECRET
// REDIRECT_URI
// // GRANT_TYPES
// SCOPE
// USER_ID
// NAME
// // DESCRIPTION
// CREATED_BY

function add_app($data) {
  global $db;
  global $token;
  global $return;
  global $content;

  if (isset($data['username']) && !empty($data['username'])) {
    $userlogin = $db->query(sprintf("SELECT USERLOGIN FROM W2SSMS_SUB_COMMUNITY WHERE USERLOGIN = '%s'", $data['username']));
    if (isset($userlogin[0]['USERLOGIN'])) {
      if (isset($data['redirect_uri']) && !empty($data['redirect_uri'])) {

        $clientsecret = $db->getrand();
        $clientid = $db->getrand();

        $field = array(
          'CLIENT_ID' => $clientid,
          'CLIENT_SECRET' => $clientsecret,
          'SCOPE' => 'user',
          'CREATED_BY' => $token['user_id'].'-'.$token['client_id'],
          'CREATED_DATE' => date("d-M-Y", time())
        );

        $field['USER_ID'] = $data['username'];
        $field['REDIRECT_URI'] = $data['redirect_uri'];
        if (isset($data['app_name']) && !empty($data['app_name'])) { $field['NAME'] = $data['app_name']; }
        $query = susunqinsert('OAUTH_CLIENTS', $field);
        $read = susunqselect('OAUTH_CLIENTS', $field);
        $client = $db->query($read);
        //var_dump($client);
        if ((count($client) > 0) && (isset($client[0]['CLIENT_ID']) && isset($client[0]['CLIENT_SECRET']))) {
	  //echo 'ada';
          $return['result'] = $client[0];
          returnFail('OK', '', $content);
        } elseif ($db->query($query, false)) {
          //echo 'tidak ada';
          $return['result'] = $field;
          returnFail('OK','',$content);
        }

      } else {
//        error_log(json_encode($data));
        returnFail('PARAMETER_INCOMPLETE', '', $content);
      }
    } else {
      returnFail('USER_NOTFOUND', 'User not found or wrong username.', $content);
    }
  } else {
//    error_log(json_encode($data));
    returnFail('PARAMETER_INCOMPLETE', '', $content);
  }
}

function inputs() {
  return array(
    'username' => v::alnum()->noWhitespace()->length(1,15),
    'redirect_uri' => v::alnum("-._~:/?#[]@!$&'()*+,;=")->noWhitespace()->notEmpty(),
    'app_name' => v::alnum('-_')->length(1,225),
  );
}

function validate($input) {
  $c = array();
  $v = inputs();

  foreach (array_keys($v) as $k) {
    if (isset($input[$k])) {
      if ($v[$k]->validate($input[$k])) {
        $c[$k] = $input[$k];
        slog('validasi field '.$k, 'true', 'Format benar', 'success');
      } else {
        //$c[$k] = '';
        slog('validasi field '.$k, 'false', 'Format salah', 'fail');
      }
    }
  }

  return $c;
}

//error_log(serialize($content));

$content = validate($content);

add_app($content);
