<?php
// include our OAuth2 Server object
require_once __DIR__.'/server.php';
$db = new Gudang();
require_once __DIR__ . '/vendor/autoload.php';
use Respect\Validation\Validator as v;

$return = array();
$input = array();

$return['timestamp'] = Date("Y-m-d H:i:s");
$return['id'] = time();
$return['status'] = 'AUTH_FAILED';
$return['groups'] = null;


// var_dump(OAuth2\Request::createFromGlobals());

$request = OAuth2\Request::createFromGlobals();
$response = new OAuth2\Response();
$content = $request->request;

$token = $server->getAccessTokenData(OAuth2\Request::createFromGlobals());

function slog($request, $retval, $desc, $event) {
  global $db;
  global $token;
  $payload = array(
    'token' => $token,
    'filename' => __FILE__,
    'request' => $request,
    'retval' => $retval,
    'desc' => $desc,
    'event' => $event
  );
  $db->saveLog($payload);
}

// Handle a request for an OAuth2.0 Access Token and send the response to the client
$scopeRequired = 'user';
if (!$server->verifyResourceRequest($request, $response, $scopeRequired)) {
  slog($content, json_encode($response->getParameters()), 'token authorize fail', 'fail');
  // echo "satu";
  $server->getResponse()->send();
  exit();
}

function returnFail($code, $reason = '', $request = null) {
  global $return;
  global $db;
  global $token;
  // var_dump($return);
  switch ($code) {
    case 'OK':
      $return['status'] = $code;
      header('Content-Type: application/json');
      $payload['retval'] = $code;
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
    case 'AUTH_FAILED':
      $return['status'] = $code;
      $return['reason'] = $reason;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
    case 'PARAMETER_INCOMPLETE':
      $return['status'] = $code;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
  }
}

$query = sprintf('
SELECT  GNAME AS GROUP_NAME,
        REG_SMS AS FORMAT_SMS,
        TO_CHAR(TO_DATE(CREATED_DATE), \'DD-MON-YYYY HH24:MI:SS\') AS CREATED_DATE,
        MAX_MEMBER AS "LIMIT",
        DESCRIPTION AS "DESC"
FROM W2SMS_GROUP
WHERE CREATED_BY = \'%s\' AND CREATED_BY IS NOT NULL
ORDER BY TO_DATE(CREATED_DATE, \'DD-MON-YYYY HH24:MI:SS\') ASC', $token['user_id']);

// var_dump($content);
// var_dump($start, $max);

// $result = array_chunk($result, $end, true);

// $content = json_decode($content->content);
// var_dump($content);

function listGroup($content) {
  global $db;
  global $query;
  global $return;

  if ((!isset($content['start']) || empty($content['start'])) ||
      (!isset($content['end'])   || empty($content['end']))) {
    returnFail('PARAMETER_INCOMPLETE', '', $content);;
  } else {
    // $content['max'] = (isset($content['max'])) ? $content['max'] : 100;
    // $content['end'] = (isset($content['end'])) ? $content['end'] : $content['max'];
    //
    // $max = $content['max'];
    // $max = ($content['end'] > $content['max']) ? $content['end'] : $max;
    $start = (isset($content['start'])) ? $content['start'] : 1;
    $end = (isset($content['end'])) ? $content['end'] : 2;
    
    // NOTE if content more than 50 make it 50, if not make it max, if not defined make it 50
    $max = (isset($content['max'])) ? ($content['max'] > 50) ? 49 : $content['max']-1 : 49;

    $sort = (isset($content['sorting'])) ? $content['sorting'] : 'ASC' ;

    if (($end - $start) <= $max) {
      // echo 'kurang';
      $end = $end;
    } else {
      // echo 'lebih';
      $end = $start + $max;
    }

    $result = $db->queryLimit($query, $start, $end, true, array("TO_DATE(CREATED_DATE, 'DD-MON-YYYY HH24:MI:SS')" => 'ASC'));
    if (strtoupper($sort) == 'DESC') {
      $result = $db->queryLimit($query, $start, $end, true, array("TO_DATE(CREATED_DATE, 'DD-MON-YYYY HH24:MI:SS')" => 'DESC'));
      // $result = array_reverse($result);
    }

    // $return['status'] = 'OK';
    $return['groups'] = $result;
    returnFail('OK','',$content);
    // echo json_encode($return);
  }
}

function inputs() {
  return array(
    'start' => v::numeric()->positive(),
    'end' => v::numeric()->positive(),
    'max' => v::numeric()->positive(),
    'sorting' => v::alpha()->noWhitespace()->length(1,4)
  );
}

function validate($input) {
  $c = array();
  $v = inputs();

  foreach (array_keys($v) as $k) {
    if (isset($input[$k])) {
      if ($v[$k]->validate($input[$k])) {
        $c[$k] = $input[$k];
        slog('validasi field '.$k, 'true', 'Format benar', 'success');
      } else {
        //$c[$k] = '';
        slog('validasi field '.$k, 'false', 'Format salah', 'fail');
      }
    }
  }

  return $c;
}

$content = validate($content);

// NOTE: 14 march 2017: no more username check
listGroup($content);
