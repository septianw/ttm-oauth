<?php
// include our OAuth2 Server object
require_once __DIR__.'/server.php';
$db = new Gudang();
require_once __DIR__ . '/vendor/autoload.php';
use Respect\Validation\Validator as v;

$return = array();
$input = array();

$return['timestamp'] = Date("Y-m-d H:i:s");
$return['id'] = time();
$return['status'] = 'AUTH_FAILED';


// var_dump(OAuth2\Request::createFromGlobals());

$request = OAuth2\Request::createFromGlobals();
$response = new OAuth2\Response();
$content = $request->request;

$token = $server->getAccessTokenData(OAuth2\Request::createFromGlobals());

function slog($request, $retval, $desc, $event) {
  global $db;
  global $token;
  $payload = array(
    'token' => $token,
    'filename' => __FILE__,
    'request' => $request,
    'retval' => $retval,
    'desc' => $desc,
    'event' => $event
  );
  $db->saveLog($payload);
}

// Handle a request for an OAuth2.0 Access Token and send the response to the client
$scopeRequired = 'user';
if (!$server->verifyResourceRequest($request, $response, $scopeRequired)) {
  slog($content, json_encode($response->getParameters()), 'token authorize fail', 'fail');
  // echo "satu";
  $server->getResponse()->send();
  exit();
}

function returnFail($code, $reason = '', $request = null) {
  global $return;
  global $db;
  global $token;
  // var_dump($return);
  switch ($code) {
    case 'OK':
      $return['status'] = $code;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
    case 'FAIL':
      $return['status'] = $code;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
  }
}

function get_resource_owner_detail($user_id) {
  global $db;
  $out = array();
  $query = sprintf("SELECT * FROM W2SSMS_SUB_COMMUNITY WHERE USERLOGIN = '%s'", $user_id);
  $outq = $db->query($query);
//  var_dump($outq);
  
  if (!empty($outq)) {
    
    $out['COMMID'] = $outq[0]['COMMID'];
    $out['USERLOGIN'] = $outq[0]['USERLOGIN'];
    $out['VALID_UNTIL'] = date('d F Y', strtotime($outq[0]['VALID_UNTIL']));
    $out['AUTORENEW_FLAG'] = ($outq[0]['AUTORENEW_FLAG'] == 'Y')? true : false;
    $out['STATUS'] = ($outq[0]['STATUS'] == 'Y') ? true : false;
    
    return $out;
  } else {
    return false;
  }
}

$resown = get_resource_owner_detail($token['user_id']);
if ($resown !== false) {
  $return['info'] = $resown;
  returnFail('OK', '', $content);
} else {
  returnFail('FAIL', '', $content);
}