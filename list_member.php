<?php
// include our OAuth2 Server object
require_once __DIR__.'/server.php';
$db = new Gudang();
require_once __DIR__ . '/vendor/autoload.php';
use Respect\Validation\Validator as v;

$return['timestamp'] = date("Y-m-d H:i:s");
$return['id'] = time();
$return['status'] = 'AUTH_FAILED';


$request = OAuth2\Request::createFromGlobals();
$response = new OAuth2\Response();

$content = $request->request;
$token = $server->getAccessTokenData($request);

function slog($request, $retval, $desc, $event) {
  global $db;
  global $token;
  $payload = array(
    'token' => $token,
    'filename' => __FILE__,
    'request' => $request,
    'retval' => $retval,
    'desc' => $desc,
    'event' => $event
  );
  $db->saveLog($payload);
}

// https://api.example.com/resource-requiring-postonwall-scope
$scopeRequired = 'user'; // this resource requires "postonwall" scope
if (!$server->verifyResourceRequest($request, $response, $scopeRequired)) {
  slog($content, json_encode($response->getParameters()), 'token authorize fail', 'fail');
  // if the scope required is different from what the token allows, this will send a "401 insufficient_scope" error
   echo 'lima';
  $response->send();
  exit();
}

function returnFail($code, $reason = '', $request = null) {
  global $return;
  global $db;
  global $token;
  // var_dump($return);
  switch ($code) {
    case 'OK':
      $return['status'] = $code;
      header('Content-Type: application/json');
      $payload['retval'] = $code;
      slog($request, json_encode($return), $reason, $code);
      echo json_encode(tranStatus($return));
    break;
    case 'AUTH_FAILED':
      $return['status'] = $code;
      $return['reason'] = $reason;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
    case 'PARAMETER_INCOMPLETE':
      $return['status'] = $code;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
  }
}

function tranStatus ($object) {
  foreach ($object as $key => $record) {
    if (is_array($record)) {
      foreach ($record as $k => $r) {
        switch ($r['STATUS']) {
          case '1':
            $object[$key][$k]['STATUS'] = 'Member Active';
          break;
          case '2':
            $object[$key][$k]['STATUS'] = 'Will Confirm';
          break;
          case '3':
            $object[$key][$k]['STATUS'] = 'Sent Not Reply';
          break;
          case '4':
            $object[$key][$k]['STATUS'] = 'Member Decline';
          break;
          case '5':
            $object[$key][$k]['STATUS'] = 'Member Quit';
          break;
          case '6':
            $object[$key][$k]['STATUS'] = 'MSISDN Expired';
          break;
        }
      }
    }
  }

  return $object;
}




// echo $content['sorting'];
$sort = (isset($content['sorting'])) ? strtoupper($content['sorting']) : 'ASC' ;
// echo $sort;

$sub_id = $db->query(sprintf("SELECT SUB_ID FROM W2SSMS_SUB_COMMUNITY WHERE USERLOGIN = '%s'", $token['user_id']));
$sub_id = $sub_id[0]['SUB_ID'];

if (isset($content['group'])) {

  $query = sprintf("
    SELECT
    M.MSISDN, M.FNAME, M.LNAME,
    TO_CHAR(M.MODIFIED_DATE, 'DD-MON-YYYY HH24:MI:SS') MODIFIED_DATE,
    TO_CHAR(M.CREATE_DATE, 'DD-MON-YYYY HH24:MI:SS') CREATE_DATE, M.STATUS
    FROM W2SMS_GROUP_MEMBER GM, W2SMS_GROUP G, W2SMS_CMR_MEMBER M
    WHERE GM.MID = M.MID AND GM.GID = G.GID AND G.GNAME LIKE '%%%s%%' AND M.SUB_ID = %s
    ORDER BY TO_DATE(CREATE_DATE, 'DD-MON-YYYY HH24:MI:SS') ASC
  ", strtoupper($content['group']), $sub_id);
} else {
  $query = sprintf("
    SELECT
    M.MSISDN, M.FNAME, M.LNAME,
    TO_CHAR(M.MODIFIED_DATE, 'DD-MON-YYYY HH24:MI:SS') MODIFIED_DATE,
    TO_CHAR(M.CREATE_DATE, 'DD-MON-YYYY HH24:MI:SS') CREATE_DATE, M.STATUS
    FROM W2SMS_CMR_MEMBER M
    WHERE M.SUB_ID = %s
    ORDER BY TO_DATE(CREATE_DATE, 'DD-MON-YYYY HH24:MI:SS') ASC
  ", $sub_id);
}

// echo $query;

// var_dump($result);

function listMember($content) {
  global $return;
  global $query;
  global $db;
  global $sort;

  if ((!isset($content['start']) || empty($content['start'])) ||
      (!isset($content['end'])   || empty($content['end']))) {
    returnFail('PARAMETER_INCOMPLETE','',$content);;
  } else {

    // $content['end'] = (isset($content['end'])) ? $content['end'] : $content['max'];
    //
    // $max = $content['max'];
    // $max = ($content['end'] > $content['max']) ? $content['end'] : $max;
    $start = (isset($content['start'])) ? $content['start'] : 1;
    $end = (isset($content['end'])) ? $content['end'] : 2;
    
    // NOTE if content more than 50 make it 50, if not make it max, if not defined make it 50
    $max = (isset($content['max'])) ? ($content['max'] > 49) ? 49 : $content['max']-1 : 49;


    if (($end - $start) <= $max) {
      // echo 'kurang';
      $end = $end;
    } else {
      // echo 'lebih';
      $end = $start + $max;
    }

    $result = $db->queryLimit($query, $start, $end, true, array("TO_DATE(CREATE_DATE, 'DD-MON-YYYY HH24:MI:SS')" => 'ASC'));
    // $result = $db->queryLimit($query, $start, $end);
    if (strtoupper($sort) == 'DESC') {
      // echo 'desc';
      // $result = array_reverse($result);
      $result = $db->queryLimit($query, $start, $end, true, array("TO_DATE(CREATE_DATE, 'DD-MON-YYYY HH24:MI:SS')" => 'DESC'));
      // $result = $db->queryLimit($query, $start, $end);
    }
    $result = $db->toLowerKey($result);

    $return['status'] = 'OK';
    $return['members'] = $result;// array_chunk($result, (isset($content['end'])) ? $content['end']: $max, true);
    returnFail('OK','',$content);
    // echo json_encode(tranStatus($return));
  }
}

function inputs() {
  return array(
    'start' => v::numeric()->positive(),
    'end' => v::numeric()->positive(),
    'max' => v::numeric()->positive(),
    'group' => v::alnum()->noWhitespace()->length(1,20),
    'sorting' => v::alpha()->noWhitespace()->length(1,4)
  );
}

function validate($input) {
  $c = array();
  $v = inputs();

  foreach (array_keys($v) as $k) {
    if (isset($input[$k])) {
      if ($v[$k]->validate($input[$k])) {
        $c[$k] = $input[$k];
        slog('validasi field '.$k, 'true', 'Format benar', 'success');
      } else {
        //$c[$k] = '';
        slog('validasi field '.$k, 'false', 'Format salah', 'fail');
      }
    }
  }

  return $c;
}

$content = validate($content);

// NOTE: 14 march 2017: no more username check
listMember($content);
