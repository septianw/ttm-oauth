<?php
// include our OAuth2 Server object
require_once __DIR__.'/server.php';
$db = new Gudang();
require_once __DIR__ . '/vendor/autoload.php';
use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;

$return['timestamp'] = date("Y-m-d H:i:s");
$return['id'] = time();
$return['smshid'] = 0;
$return['status'] = 'AUTH_FAILED';

// get next ID
$idresult = $db->query("SELECT SEQ_SMS_SCHEDULE_SCHEDULEID.NEXTVAL AS INSERT_ID FROM DUAL");
$idresult = $idresult[0]['INSERT_ID']+1;

$request = OAuth2\Request::createFromGlobals();
$response = new OAuth2\Response();
$content = $request->request;

$token = $server->getAccessTokenData(OAuth2\Request::createFromGlobals());

function slog($request, $retval, $desc, $event) {
  global $db;
  global $token;
  $payload = array(
    'token' => $token,
    'filename' => __FILE__,
    'request' => $request,
    'retval' => $retval,
    'desc' => $desc,
    'event' => $event
  );
  $db->saveLog($payload);
}

// Handle a request for an OAuth2.0 Access Token and send the response to the client
$scopeRequired = 'user';
if (!$server->verifyResourceRequest($request, $response, $scopeRequired)) {
  slog($content, json_encode($response->getParameters()), 'token authorize fail', 'fail');
  // echo "satu";
  $server->getResponse()->send();
  exit();
}

/**
 * ReturnFail
 * Return code to end user
 * @param String        $code     Code of return, consist of OK, AUTH_FAILED, NOT_MEMBER, NO_MSISDN, ERROR
 * @param Array|String  $reason   Description of why error
 * @param Array|Null    $request  Input request.
 * @return  
 */
function returnFail($code, $reason = '', $request = null) {
  global $return;
  global $db;
  global $token;
  // var_dump($return);
  switch ($code) {
    case 'OK':
      $return['status'] = $code;
      $payload['retval'] = json_encode($return);
//      if (isset($reason)) { $return['reason'] = $reason; }
      if (is_array($reason)) { $reason = json_encode($reason); }
      slog($request, json_encode($return), $reason, $code);
      header('Content-Type: application/json');
      echo json_encode($return);
    break;
    case 'AUTH_FAILED':
      $return['status'] = $code;
      $return['reason'] = $reason;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
    case 'NOT_MEMBER':
      $return['status'] = $code;
      $return['reason'] = $reason;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
    case 'NO_MSISDN':
      $return['status'] = $code;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
    case 'ERROR':
      $return['status'] = $code;
      $return['reason'] = $reason;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
  }
}

/**
 * Format MSISDN to standard
 * @param   String $msisdn  Unformated MSISDN input
 * @return  String          Formated MSISDN
 */
function formatMsisdn($msisdn) {
  if (substr($msisdn, 0, 1) == '8') {
    return $msisdn;
  } elseif (substr($msisdn, 0, 2) == '08') {
    return substr($msisdn, 1);
  } elseif (substr($msisdn, 0, 3) == '628') {
    return substr($msisdn, 2);
  }
}

$queryRespon = "
SELECT DISTINCT
  E.MSGTXT,
  B.SEND_DATE,
  B.SMS_SENT,
  STS.SUCCESS,
  STP.PENDING,
  B.SMS_FAIL,
  BUCKET.REM_QUOTA,
  B.SMS_EXCESS
FROM
  W2SMS_SMS_HEADER B
JOIN  W2SMS_SEND E
  ON B.SMSHID = E.SMSHID
JOIN (
        SELECT SMSHID, COUNT(DECODE(STATUS, 'SUCCESS', 1)) SUCCESS
        FROM W2SMS_SEND
        GROUP BY SMSHID
      ) STS
  ON STS.SMSHID = B.SMSHID
JOIN (
        SELECT SMSHID, COUNT(DECODE(STATUS, 'ON SCHEDULE', 1)) PENDING
        FROM W2SMS_SEND
        GROUP BY SMSHID
      ) STP
  ON STP.SMSHID = B.SMSHID
JOIN (
        SELECT SUB_ID, BUCKET-BUCKET_USAGE REM_QUOTA
        FROM W2SMS_BUCKET_SMS
      ) BUCKET
  ON BUCKET.SUB_ID = B.SUB_ID
JOIN W2SSMS_SUB_COMMUNITY \"USER\"
  ON \"USER\".SUB_ID = B.SUB_ID
WHERE \"USER\".USERLOGIN = '%s'
";

function executePlsqlFunction($func) {
  global $db;
  $dbid = $db->noPdo();

  $query = sprintf("begin :r := %s; end;", $func);
  
//  var_dump($query);

  $stid = oci_parse($dbid, $query);
  oci_bind_by_name($stid, ':r', $res, 400);
  oci_execute($stid);
  oci_free_statement($stid);
  
//  var_dump($res);

  return $res;
}

/**
 * Kirim is broadcast_permember_header procedure wrapper
 * Written here to make calling procedure comfortable
 * @param Int     $idresult     Id to be inserted
 * @param String  $commid       Community ID
 * @param String  $description  Description of SMS
 * @param String  $msisdn       PIC MSISDN
 * @param Int     $sub_id       Subscriber ID
 * @param String  $uname        Username
 * @param Int     $durasi       Duration
 * @param String  $p_type       p_type
 * @param String  $waktu_kirim  Time of sent
 * @return  
 */
function kirim ($idresult,$commid,$description,$msisdn,$sub_id,$uname,$durasi,$p_type,$waktu_kirim) {
  global $db;
//  var_dump($idresult,$commid,$description,$msisdn,$sub_id,$uname,$durasi,$p_type,$waktu_kirim); die();

  // $stid = oci_parse($dbid,"begin :r := broadcast_permember_header(:p1,:p2,:p3,:p4,:p5,:p6,:p7,:p8, TO_DATE(:p9, 'DD-MM-YYYY HH24:MI' )); end;");
  //   oci_bind_by_name($stid, ':p1', $idresult);
  //   oci_bind_by_name($stid, ':p2', $commid);
  //   oci_bind_by_name($stid, ':p3', $description);
  //   oci_bind_by_name($stid, ':p4', $msisdn);
  //   oci_bind_by_name($stid, ':p5', $sub_id);
  //   oci_bind_by_name($stid, ':p6', $uname);
  //   oci_bind_by_name($stid, ':p7', $durasi);
  //   oci_bind_by_name($stid, ':p8', $p_type);
  //   oci_bind_by_name($stid, ':p9', $waktu_kirim);
  //
  //   oci_bind_by_name($stid, ':r', $r, 4000);
  //   // $db->dumpToLog($stid);
  //   oci_execute($stid);
  //   oci_free_statement($stid);
  //
  //   return $r;

  // $query = sprintf("begin :r := broadcast_permember_header(2136,'SLO','Hello Wrong :)','8111011199',110200,'tello',1,1,null); end;");
  // FIXME: ganti ke bind parameter.
  $query = sprintf("broadcast_permember_header(%d,   '%s','%s',            '%s',        %d,    '%s',   %d,%d,to_date('%s', 'DD-MM-YYYY HH24:MI:SS'))",
    $idresult,$commid,$description,$msisdn,$sub_id,$uname,$durasi,$p_type,$waktu_kirim);

  $r = executePlsqlFunction($query);
  
  return $r;
}


// FIXME: ganti ke bind parameter
/**
 * Build SQL structure
 * @param   String $mode  Mode to find, consist of stats, and msg
 * @param   String $param Additional param to query
 * @return                Query return
 */
function susunquery ($mode, $param) {

  $mainp = "
  FROM
    W2SMS_SMS_HEADER B
  JOIN  W2SMS_SEND E
    ON B.SMSHID = E.SMSHID
  JOIN (
          SELECT SMSHID, COUNT(DECODE(STATUS, 'SUCCESS', 1)) SUCCESS
          FROM W2SMS_SEND
          GROUP BY SMSHID
        ) STS
    ON STS.SMSHID = B.SMSHID
  JOIN (
          SELECT SMSHID, COUNT(DECODE(STATUS, 'ON SCHEDULE', 1)) PENDING
          FROM W2SMS_SEND
          GROUP BY SMSHID
        ) STP
    ON STP.SMSHID = B.SMSHID
  JOIN (
          SELECT SUB_ID, BUCKET-BUCKET_USAGE REM_QUOTA
          FROM W2SMS_BUCKET_SMS
        ) BUCKET
    ON BUCKET.SUB_ID = B.SUB_ID
  JOIN W2SSMS_SUB_COMMUNITY \"USER\"
    ON \"USER\".SUB_ID = B.SUB_ID
  WHERE \"USER\".USERLOGIN = '%s'
  ";

  switch ($mode) {
    case 'stats':
      $prefix = "
      SELECT
        SUM(B.SMS_SENT) SMS_SENT,
        SUM(STS.SUCCESS) SUCCESS,
        SUM(STP.PENDING) PENDING,
        SUM(B.SMS_FAIL) FAIL,
        BUCKET.REM_QUOTA,
        B.SMS_EXCESS";
      $postfix = "GROUP BY BUCKET.REM_QUOTA, B.SMS_EXCESS";
      $query = sprintf($prefix.$mainp.$postfix, $param);
      // return $query;
    break;
    case 'msg':
      $prefix = "
      SELECT
        TO_CHAR(TO_DATE(B.SEND_DATE),  'YYYY-MM-DD HH24:MI:SS') SEND_DATE,
        E.MSGTXT";
      $postfix = "ORDER BY SEND_DATE";
      $query = sprintf($prefix.$mainp.$postfix, $param);
      // return $query
    break;
  }

  return $query;
}


/**
 * Insert MSISDN to queue sms header
 * @param int     $smshid SMS header id
 * @param int     $sub_id subscriber id
 * @param string  $msisdn msisdn separated by comma
 * @return  
 */
function insert_sms_member($smshid,$sub_id,$msisdn, $picmsisdn, $msg) {
  global $db;
//  $dbobj = $db->getDb();
//  $dbobj->beginTransaction();
  $result = false;
  if (strpos($msisdn, ',') === false) {
    $msisdn = formatMsisdn($msisdn);
    $que = sprintf("broadcast_permember_detail(%d,%d,'%s')", $smshid, $sub_id, $msisdn);
    $res = executePlsqlFunction($que);
//    var_dump($que); die();
    
    if ($res == 'OK') {
//      $dbobj->commit();
//      insertTemp($smshid, $msisdn, $picmsisdn, $sub_id, $msg);
      return true;
    } else {
      return false;
    }
  } else {
    $listnum = explode(',', $msisdn);
    foreach ($listnum as $key => $value) {
      $value = formatMsisdn($value);
      $que = sprintf("broadcast_permember_detail(%d,%d,'%s')", $smshid, $sub_id, $msisdn);
      $res = executePlsqlFunction($que);
      // $exe = $dbobj->prepare(sprintf("INSERT INTO W2SMS_SMS_MEMBER (SMSHID, SUB_ID, MSISDN) VALUES (%d, %d, '%s')",$smshid, $sub_id, $value));
      if ($res == 'OK') {
//        insertTemp($smshid, $msisdn, $picmsisdn, $sub_id, $msg);
        $result = true;
      } else {
        $result = false;
        break;
      }
    }
    return $result;
  }
}

/**
 * Alternative to file_get_content
 * @param   String $Url String of full URL
 * @return  String      String return from URL
 */
function url_get_contents($Url) {
    if (!function_exists('curl_init')){
        die('CURL is not installed!');
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $Url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}

/**
 * Check if input MSISDN is a member of a group.
 * @param   String  $msisdn
 * @return  Boolean
 */
function isMember($msisdn) {
  // SELECT * FROM
//  W2SMS_GROUP_MEMBER
//WHERE
//  MSISDN = '81333456789'
  global $db;
  $q = sprintf('SELECT * FROM W2SMS_GROUP_MEMBER WHERE MSISDN = \'%s\' AND STATUS = 1', $msisdn);
  $out = $db->query($q);
  if (empty($out)) {
    return false;
  } else {
    return true;
  }
}

/**
 * Check if MSISDN is active member
 * @param   String $msisdn MSISDN number of member
 * @return  Boolean
 */
function isMsisdnActive($msisdn) {
  global $db;
  $out = false;
  
  $msisdnActive = $db->query(sprintf("SELECT COUNT(*) ACTIVE FROM W2SMS_CMR_MEMBER WHERE STATUS = 1 AND MSISDN = '%s'", $msisdn));
  if ($msisdnActive[0]['ACTIVE'] >= 1) {  // Execution end here. Get out of this function
    $out = true;
  }
  
  return $out;
}

//function insertTemp ($mid, $msisdn, $picmsisdn, $subid, $msg) {
//  global $db;
//  $que = $db->susunqinsert('TEMP_INSERT', array(
//    'MID' => "'$mid'",
//    'MSISDN' => "'$msisdn'",
//    'TYPE_INSERT' => "'smstomember'",
//    'REF_MSISDN' => "'$picmsisdn'",
//    'SUB_ID' => $subid,
//    'MSGTXT' => "'$msg'"
//  ));
////  var_dump($que); die();
//  
//  return $db->query($que, false);
//}

//function cleanTemp ($picmsisdn, $smshid) {
//  global $db;
////  return $db->query(sprintf("DELETE FROM TEMP_INSERT WHERE TYPE_INSERT = 'smstomember' AND REF_MSISDN = '%s' AND MID = '%s'", $picmsisdn, $smshid));
//  return null;
//}

/**
 * Send to member main function.
 * This function will actually send SMS to MSISDN
 * This function must handle comma separated MSISDN
 * 
 * This function will do
 *   - Check if Account active
 *   - Get keyword <- NOTE: hardcoded on URL
 *   - Put header SMS
 *   - Put detail SMS
 * 
 * @param   Array $content  Array of input content
 * @param   Int   &$smshid  Reference to smshid out there.
 * @return  Array           Array of sending status
 */
function sendToMember ($content, &$smshid) {
 
  global $db;
  global $token;
  global $idresult;
  
  // Check if account active.
  $userQuery = $db->query(
    "SELECT USERLOGIN, COMMID, SUB_ID, PIC_MSISDN FROM W2SSMS_SUB_COMMUNITY WHERE USERLOGIN = :ulogin AND STATUS = 'Y'",
    true,
    array(array(':ulogin', $token['user_id'], PDO::PARAM_STR, 15))
  );
  if (count($userQuery) == 0) { // Execution end here. Get out of this function
    Util::KeepSmsLog($idresult, $userQuery[0]['PIC_MSISDN'], 'Not Active', 'Your account is not active.');
    return false;
  }
  
  $smshid = (int)$idresult;

  $q = $db->query(
    "SELECT KEYWORD FROM W2SMS_KEYWORDS WHERE KEYWORD_ID = :keywordid",
    true,
    array(array(':keywordid', '1012', PDO::PARAM_STR, 4))
  );
  $format_broadcast = $q[0]['KEYWORD'];

  // Set structure for header
  $sub_id = $userQuery[0]['SUB_ID']; // 110200
  $commid = $userQuery[0]['COMMID'];

  $uname = $content['username'];// ($content['username'] == $query[0]['USERLOGIN']) ? $content['username'] : $query[0]['USERLOGIN'];
  $durasi = 2;
  $p_type = 1;
  $description = (isset($content['message'])) ? $content['message'] : '';
  $time = time();
  $waktu_kirim = date('d-m-Y H:i', $time);

  $r = kirim($idresult,$commid,$description,$userQuery[0]['PIC_MSISDN'],$sub_id,$uname,$durasi,$p_type,$waktu_kirim);

//   var_dump($r); die(); // HACK

  // if OK, queueing sms detail
  if ($r == 'OK') {
    // $smshid = $db->query("SELECT SEQ_W2SMS_HEADER.CURRVAL AS INSERT_ID FROM DUAL");
    // $smshid = $smshid[0]['INSERT_ID'];
    $resout = array();
    if (strpos($content['msisdn'], ',') !== false) {
      foreach (explode(',', $content['msisdn']) as $key => $val) {
        if (insert_sms_member($idresult,$sub_id,$val, $userQuery[0]['PIC_MSISDN'], $description)) {
          Util::KeepSmsLog($idresult, $val, 'SUCCESS', '', $description);
        } else {
          break;
          Util::KeepSmsLog($idresult, $val, 'ERROR', sprintf('Unable queueing sms member. Transaction stalled on ID %s', $idresult), $description);
        }
      }
//      cleanTemp($userQuery[0]['PIC_MSISDN'], $idresult);
    } else {
      if (!insert_sms_member($idresult,$sub_id,$content['msisdn'], $userQuery[0]['PIC_MSISDN'], $description)) {
        Util::KeepSmsLog($idresult, $content['msisdn'], 'ERROR', sprintf('Unable queueing sms member. Transaction stalled on ID %s', $idresult), $description);
        return false;
      } else {
        Util::KeepSmsLog($idresult, $content['msisdn'], 'SUCCESS', '', $description);
      }
    }
    
    // activate when ready
    // production
//    $url = sprintf("http://10.1.89.201:8081/commetmo/SmsMO?channel=WEB&adn=2324&msisdn=62%s&msg=CMBC_MEMBER %d %d", $query[0]['PIC_MSISDN'], $idresult, $sub_id);
    // development
    $url = sprintf("http://10.2.117.80:8080/commetmo/SmsMO?channel=WEB&adn=2324&msisdn=62%s&msg=%s %d %d",$userQuery[0]['PIC_MSISDN'], $format_broadcast, $idresult, $sub_id);
    
// $ins = $this->db->query("INSERT INTO W2SMS_MESSAGE_TEMP(SMSHID,MSGTXT,CREATE_DATE) VALUES ($idresult,'$descriptionx',SYSDATE)");
// $result = file_get_contents("http://10.2.117.80:8080/commetmo/SmsMO?channel=WEB&adn=2324&msisdn=".urlencode($msisdn)."&msg=".urlencode($    format_broadcast)."%20%23".urlencode($gformat)."%20".urlencode($idresult));
    

    $url = str_replace(" ", '%20', $url);
    $db->dumpToLog(__LINE__,$url);
    $sent = url_get_contents($url);  // NOTE comment this on dev server
//    $sent = true; // NOTE uncomment for development phase

//    var_dump($sent, $url); die();

    if ($sent === false) {
      if (strpos($content['msisdn'], ',') !== false) {
        foreach(explode(',', $content['msisdn']) as $key => $val) {
          Util::KeepSmsLog($idresult, $val, 'FAIL', 'Unable to contact commet SmsMO gateway.', $description);
          return false;
        }
      } else {
        Util::KeepSmsLog($idresult, $val, 'FAIL', 'Unable to contact commet SmsMO gateway.', $description);
        return false;
      }
    } else {
      return $resout;
    }
  } else {
    var_dump($r, "wow"); die();
  }
}

function inputs() {
  return array(
    'msisdn' => v::notEmpty()->digit(','),
    'message' => v::alnum(",.?'\"!()[]")->length(1, 1000)
  );
}

/**
 * Validation againts user input.
 * Invalid input will omited.
 * 
 * @param   Array $input  Array of user input.
 * @return  Array         Array of validated user input.
 */
function validate($input) {
  global $db;
  $c = array();
  $v = inputs();

  foreach (array_keys($v) as $k) {
    if (isset($input[$k])) {
      if ($v[$k]->validate($input[$k])) {
        $c[$k] = $input[$k];
        slog('validasi field '.$k, 'true', 'Format benar', 'success');
      } else {
        //$c[$k] = '';
        slog('validasi field '.$k, 'false', 'Format salah', 'fail');
      }
    }
  }

  return $c;
}

$content = validate($content);
//var_dump($content); die();
if (isset($content['message'])) {
  $smshid = 0;

  // NOTE: 14 march 2017: no more username check
  $content['username'] = $token['user_id'];
  $content['message'] = (isset($content['message'])) ? $content['message'] : '';
  if (isset($content['msisdn']) && !empty($content['msisdn'])) {
    if (strpos($content['msisdn'], ',') === false) {
  //    echo 'no comma in it';
      $content['msisdn'] = formatMsisdn($content['msisdn']);
      if (isMsisdnActive($content['msisdn'])) {
        $out = sendToMember($content, $smshid);
        $return['smshid'] = $smshid;
        $content['message'] = substr($content['message'], 0, 125).'... Content omitted ...';
        returnFail('OK', '', $content);
      } else {
        $return['smshid'] = $idresult;
        $content['message'] = substr($content['message'], 0, 125).'... Content omitted ...';
        Util::KeepSmsLog($idresult, $content['msisdn'], 'Not Active', 'Member not active.', $content['message']);
        returnFail('OK', '', $content);
      }
    } else {
  //    echo 'comma in it';
      /*
       * Kalau ada koma, maka:
       *   - Pecah jadi array
       *   - format msisdn dan kembalikan ke array
       *   - Check if MSISDN active
       *   - Check if is member
       **/
      $state = true;
      $out = array();
      $sentmsisdn = array();
      $activeMsisdn = array();
      
      // explode, format, check active, make a note
      
      $cont = $content;
      if (strpos($content['msisdn'], ',') !== false) {
        $cmsisdn = explode(',', $content['msisdn']);
        foreach($cmsisdn as $key => $val) {
          $val = formatMsisdn($val);
          if (isMsisdnActive($val)) {
            $activeMsisdn[] = $val;
          } else {
            Util::KeepSmsLog($idresult, $val, 'FAIL', 'Member not active.', $content['message']);
          }
        }
        $cont['msisdn'] = implode(',', $activeMsisdn);
      } else {
        $val = formatMsisdn($content['msisdn']);
        if (!isMsisdnActive($val)) {
          Util::KeepSmsLog($idresult, $val, 'Not Active', 'Member not active.', $content['message']);
        }
      }
      
  //    $mOut = sendToMember($cont, $smshid);  // real send, return array result of every MSISDN.
      sendToMember($cont, $smshid);  // real send, return array result of every MSISDN.
      $return['smshid'] = $smshid;
  //    var_dump($mOut, $out); die();
  //    if ((isset($mOut['STATUS'])) && ($mOut['STATUS'] === 'Not Active')) {
  //      $out = array($mOut);
  //    } else {
  //      $out = array_merge($out, $mOut);
  //    }
      $content['message'] = substr($content['message'], 0, 125).'... Content omitted ...';
      returnFail('OK', '', $content);
    }
  } else {
    returnFail('NO_MSISDN', '', $content);
  }
} else {
  returnFail('ERROR', 'Message contain invalid character.', $content);
}
