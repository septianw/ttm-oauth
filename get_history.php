<?php
// include our OAuth2 Server object
require_once __DIR__.'/server.php';
$db = new Gudang();
require_once __DIR__ . '/vendor/autoload.php';
use Respect\Validation\Validator as v;

$return['timestamp'] = date("Y-m-d H:i:s");
// $return['id'] = time();
$return['status'] = null;


$request = OAuth2\Request::createFromGlobals();
$response = new OAuth2\Response();
$content = $request->request;

$token = $server->getAccessTokenData(OAuth2\Request::createFromGlobals());

function slog($request, $retval, $desc, $event) {
  global $db;
  global $token;
  $payload = array(
    'token' => $token,
    'filename' => __FILE__,
    'request' => $request,
    'retval' => $retval,
    'desc' => $desc,
    'event' => $event
  );
  $db->saveLog($payload);
}

// Handle a request for an OAuth2.0 Access Token and send the response to the client
$scopeRequired = 'user';
if (!$server->verifyResourceRequest($request, $response, $scopeRequired)) {
  slog($content, json_encode($response->getParameters()), 'token authorize fail', 'fail');
  // echo "satu";
    $server->getResponse()->send();
    exit();
}

function returnFail($code, $reason = '', $request = null) {
  global $return;
  global $db;
  global $token;
  // var_dump($return);
  switch ($code) {
    case 'OK':
      $return['status'] = $code;
      if (!empty($reason)) {
        $return['reason'] = $reason;
      }
      header('Content-Type: application/json');
      $payload['retval'] = $code;
      slog($request, json_encode($return), $reason, $code);
      $return = Util::validateOutput($return);
      echo json_encode($return);
    break;
    case 'AUTH_FAILED':
      $return['status'] = $code;
      $return['reason'] = $reason;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      $return = Util::validateOutput($return);
      echo json_encode($return);
    break;
    case 'PARAMETER_INCOMPLETE':
      $return['status'] = $code;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      $return = Util::validateOutput($return);
      echo json_encode($return);
    break;
  }
}

function susunquery ($mode, $userlogin, $hid = null, $sort = 'ASC') {

  $mainp = "
  FROM
    W2SMS_SMS_HEADER B
  JOIN  W2SMS_SEND E
    ON B.SMSHID = E.SMSHID
  JOIN (
          SELECT SMSHID, COUNT(DECODE(STATUS, 'SUCCESS', 1)) SUCCESS
          FROM W2SMS_SEND
          GROUP BY SMSHID
        ) STS
    ON STS.SMSHID = B.SMSHID
  JOIN (
          SELECT SMSHID, COUNT(DECODE(STATUS, 'ON SCHEDULE', 1)) PENDING
          FROM W2SMS_SEND
          GROUP BY SMSHID
        ) STP
    ON STP.SMSHID = B.SMSHID
  JOIN (
          SELECT SMSHID, COUNT(DECODE(STATUS, 'FAIL', 1)) FAIL
          FROM W2SMS_SEND_ERR
          GROUP BY SMSHID
        ) STF
    ON STF.SMSHID = B.SMSHID
  JOIN (
          SELECT SUB_ID, BUCKET-BUCKET_USAGE REM_QUOTA
          FROM W2SMS_BUCKET_SMS
        ) BUCKET
    ON BUCKET.SUB_ID = B.SUB_ID
  JOIN W2SSMS_SUB_COMMUNITY \"USER\"
    ON \"USER\".SUB_ID = B.SUB_ID
  WHERE \"USER\".USERLOGIN = '%s'
  ";

  if (!is_null($hid)) {
    $mainp .= " AND B.SMSHID = ".$hid." ";
  }

  switch ($mode) {
    case 'stats':
      $prefix = "
      SELECT
        SUM(STS.SUCCESS)+SUM(STP.PENDING)+SUM(STF.FAIL) SMS_SENT,
        SUM(STS.SUCCESS) SUCCESS,
        SUM(STP.PENDING) PENDING,
        SUM(STF.FAIL) FAIL,
        BUCKET.REM_QUOTA,
        B.SMS_EXCESS";
      $postfix = "GROUP BY BUCKET.REM_QUOTA, B.SMS_EXCESS";
      $query = sprintf($prefix.$mainp.$postfix, $userlogin);
      // return $query;
    break;
    case 'msg':
      $prefix = "
      SELECT DISTINCT
        B.SMSHID id,
        TO_CHAR(TO_DATE(B.SEND_DATE),  'YYYY-MM-DD HH24:MI:SS') SEND_DATE,
        E.MSGTXT";
      $postfix = "ORDER BY TO_DATE(SEND_DATE, 'YYYY-MM-DD HH24:MI:SS') ASC";
      $query = sprintf($prefix.$mainp.$postfix, $userlogin);
      // return $query
    break;
  }

  return $query;
}

//function susunquery ($mode, $userlogin, $hid = null, $sort = 'ASC') {
function squery ($type, $userlogin, $hid = null, $sort = 'ASC') {
  switch ($type) {
    case "msg":
      $field = "B.SMSHID ID, SEND_DATE, B.CREATE_DATE, DESCRIPTION MSGTXT, SUCCESS, PENDING, NVL(FAIL, 0) FAIL";
      break;
    case "stat":
      $field = "SEND_DATE, B.BUCKET BUCKET, B.BUCKET_USAGE BUCKET_USAGE, B.BUCKET - B.BUCKET_USAGE REMQUOTA";
      break;
  }
  $query = sprintf("SELECT %s
    FROM W2SMS_SMS_HEADER B
    JOIN (
      SELECT SMSHID, COUNT(DECODE(STATUS, 'SUCCESS', 1)) SUCCESS
        FROM W2SMS_SEND
      GROUP BY SMSHID
    ) SEN
      ON SEN.SMSHID = B.SMSHID

    LEFT JOIN (
      SELECT SMSHID, COUNT(DECODE(STATUS, 'FAIL', 1)) FAIL
        FROM W2SMS_SEND_ERR
      GROUP BY SMSHID
    ) FAI
      ON FAI.SMSHID = B.SMSHID

    JOIN (
      SELECT SMSHID, COUNT(DECODE(STATUS, 'ON SCHEDULE', 1)) PENDING
        FROM W2SMS_SEND
      GROUP BY SMSHID
    ) PEN
      ON PEN.SMSHID = B.SMSHID

    JOIN W2SSMS_SUB_COMMUNITY U
      ON U.SUB_ID = B.SUB_ID

    WHERE U.USERLOGIN = '%s'", $field, $userlogin);
  if (isset($hid)) {
    $query .= sprintf(" AND B.SMSHID = %d", $hid);
  }
  $query .= sprintf("ORDER BY TO_DATE(SEND_DATE, 'YYYY-MM-DD HH24:MI:SS') %s", $sort);
  return $query;
}

function getBucketStatus($smshid) {
  global $db;
  $query = sprintf("SELECT SMS_SENT, SMS_EXCESS, SMS_FAIL, BUCKET, BUCKET_USAGE FROM W2SMS_SMS_HEADER WHERE SMSHID = %d", $smshid);
  $out = $db->query($query);
  return $out[0];
}

function mergeResult($msg, $stat) {
  $history = array();
  foreach ($msg as $key => $value) {
    unset($stat[$key]['SEND_DATE']);
    $history[$key] = array_merge($msg[$key], $stat[$key]);
  }
  return $history;
}


// butuhnya gini
/*  {
      "number": 1,
      "smshid": "3170",
      "send_date": "2017-05-04 00:00:00",
      "create_date": "2017-05-04 00:00:00",
      "msgtxt": "Test QC Teman Telkomsel\nPIC:081233559111",
      
      "success": "2", 
      "lsuccess":  [{
           "msisdn": "08xxxx",
           "reason": "success"
        },
        {
           "msisdn": "08xxxx",
           "reason": "success"
        }],
      "pending": "0",
      "fail": "1",
      "lfail": [{
           "msisdn": "0812xxx",
           "reason": "no tidak terdaftar/no tidak aktif/bukan nomor tsel"
        }], 
      "rem_quota": "pengurangn bucket dengan bucket_usage ",
    }*/
// adanya gini
/*  {
      "NUMBER": 1,
      "ID": "3170",
      "SEND_DATE": "2017-05-04 00:00:00",
      "MSGTXT": "Test QC Teman Telkomsel\nPIC:081233559111",
      "sms_sent": "1",
      "success": "1",
      "pending": "0",
      "fail": "0",
      "rem_quota": "1851",
      "sms_excess": "0"
    }*/
function getHistory($content) {
  global $db;
  // global $start;
  // global $max;
  global $return;

//  var_dump(susunquery('stats', $content['username']));

  if (isset($content['id'])) {
    // echo susunquery('msg', $content['username'],$sort,$content['id']);
//    $que = susunquery('msg', $content['username'], $content['id']);
//    $que = squery($content['username'], $content['id']);
    $history = mergeResult(
        $db->query(squery('msg', $content['username'], $content['id'])),
        $db->query(squery('stat', $content['username'], $content['id']))
    );
//    echo $que; die();
//    $history = $db->query($que);
  } else {
    $content['sorting'] = (isset($content['sorting'])) ? $content['sorting'] : 'ASC' ;

    $start = (isset($content['start'])) ? $content['start'] : 1;
    $end = (isset($content['end'])) ? $content['end'] : 2;
    
    // NOTE if content more than 50 make it 50, if not make it max, if not defined make it 50
    $max = (isset($content['max'])) ? ($content['max'] > 49) ? 49 : $content['max']-1 : 49;

    if (($end - $start) <= $max) {
      // echo 'kurang';
      $end = $end;
    } else {
      // echo 'lebih';
      $end = $start + $max;
    }

    if (strtoupper($content['sorting']) == 'DESC') {
      $queMsg = squery('msg', $content['username'], null);
      $queStat = squery('stat', $content['username'], null);
//      var_dump($que); die();
      $hMsg = $db->queryLimit($queMsg, $start,$end, true, array("TO_DATE(SEND_DATE, 'YYYY-MM-DD HH24:MI:SS')"=>'DESC'));
      $hStat = $db->queryLimit($queStat, $start,$end, true, array("TO_DATE(SEND_DATE, 'YYYY-MM-DD HH24:MI:SS')"=>'DESC'));
      $history = mergeResult($hMsg, $hStat);
      // $history = array_reverse($history);
    } else {
      $queMsg = squery('msg', $content['username'], null);
      $queStat = squery('stat', $content['username'], null);
//      var_dump($que); die();
      $hMsg = $db->queryLimit($queMsg, $start,$end, true, array("TO_DATE(SEND_DATE, 'YYYY-MM-DD HH24:MI:SS')"=>'ASC'));
      $hStat = $db->queryLimit($queStat, $start,$end, true, array("TO_DATE(SEND_DATE, 'YYYY-MM-DD HH24:MI:SS')"=>'ASC'));
      $history = mergeResult($hMsg, $hStat);
      // public function queryLimit($q, $start, $end, $fetch = true, $order = null, $bindparam = null) {
    }
  }

//  var_dump($status);
//  var_dump($history); die();
  if ($history) {
    // $return = array_merge($return,$db->toLowerKey($status[0]));
    foreach($history as $key => $value) {
      $vin = array();
      $lsuccess = $db->query(sprintf("SELECT MSISDN, STATUS FROM W2SMS_SEND WHERE SMSHID = %d", $value['ID']));
      $lfail = $db->query(sprintf("SELECT MSISDN, STATUS, ERR_DESC REASON FROM W2SMS_SEND_ERR WHERE SMSHID = %d", $value['ID']));
//      $status = $db->query(susunquery('stats', $content['username'], $value['ID']));
      foreach ($value as $keyin => $valuein) {
        $vin[$keyin] = $valuein;
        
        if ($keyin == 'SUCCESS') {
          $vin['LSUCCESS'] = $lsuccess;
        }
        
        if ($keyin == 'FAIL') {
          $vin['LFAIL'] = $lfail;
        }
      }
//      $value['LSUCCESS'] = $lsuccess;
//      $value['LFAIL'] = $lfail;
      $history[$key] = $vin;
    }
    $return['histories'] = $history;
    returnFail('OK','',$content);
  } else {
    returnFail('OK', 'Empty result', $content);
  }
}

function inputs() {
  return array(
    'start' => v::numeric()->positive(),
    'end' => v::numeric()->positive(),
    'max' => v::numeric()->positive(),
    'id' => v::numeric()->positive()
  );
}

function validate($input) {
  $c = array();
  $v = inputs();

  foreach (array_keys($v) as $k) {
    if (isset($input[$k])) {
      if ($v[$k]->validate($input[$k])) {
        $c[$k] = $input[$k];
        slog('validasi field '.$k, 'true', 'Format benar', 'success');
      } else {
        //$c[$k] = '';
        slog('validasi field '.$k, 'false', 'Format salah', 'fail');
      }
    }
  }

  return $c;
}

$content = validate($content);

// NOTE: 14 march 2017: no more username check

$content['username'] = $token['user_id'];
// getHistory($content);
if (isset($content['id'])) {
  // ada id berarti start end diabaikan
  getHistory($content);
  // echo 'ada id';
} else {
  // echo 'uji start end';
//  var_dump((isset($content['start'])) && (isset($content['end'])));
  if ((isset($content['start']) && isset($content['end'])) || isset($content['id'])) {
    // echo 'ada start end';
    getHistory($content);
  } else {
    // echo 'salah satu gak ada';
    returnFail('PARAMETER_INCOMPLETE', '', $content);
  }
}
