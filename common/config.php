<?php

$tns = "
(DESCRIPTION =
  (
    ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = %s)(PORT = %s))
  )
  (
    CONNECT_DATA = (SERVICE_NAME = %s)
  )
)";


function get_config() {
  global $tns;
  $filename = join(DIRECTORY_SEPARATOR, array(__DIR__, 'config.conf'));
  $fh = fopen($filename, "r");
  $isi = fread($fh, filesize($filename));
  fclose($fh);

  $config = explode("\n", $isi);
  array_pop($config);
  foreach ($config as $value) {
    $cnf[substr($value,0,strpos($value,':'))] = substr($value,strpos($value,' ')+1,strlen($value));
  }

  $config = array(
    'dsn' => "oci:dbname=//10.2.117.80:1521/indgrid3",
    'param' => array(
      'host' => $cnf['host'],
      'port' => $cnf['port'],
      'sid' => $cnf['sid'],
    ),
    'username' => $cnf['user'],
    'password' => $cnf['pass'],
    'webhost' => $cnf['webhost']
  );

  return $config;
}
