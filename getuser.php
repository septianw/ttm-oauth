<?php
// include our OAuth2 Server object
require_once __DIR__.'/server.php';
$db = new Gudang();
require_once __DIR__ . '/vendor/autoload.php';
use Respect\Validation\Validator as v;


$request = OAuth2\Request::createFromGlobals();
$response = new OAuth2\Response();
$content = $request->query;

$token = $server->getAccessTokenData($request);

function slog($request, $retval, $desc, $event) {
  global $db;
  global $token;
  $payload = array(
    'token' => $token,
    'filename' => __FILE__,
    'request' => $request,
    'retval' => $retval,
    'desc' => $desc,
    'event' => $event
  );
  $db->saveLog($payload);
}

// https://api.example.com/resource-requiring-postonwall-scope
$scopeRequired = 'apps'; // this resource requires "postonwall" scope
if (!$server->verifyResourceRequest($request, $response, $scopeRequired)) {
  slog($content, json_encode($response->getParameters()), 'token authorize fail', 'fail');
  // if the scope required is different from what the token allows, this will send a "401 insufficient_scope" error
  // echo 'lima';
  $response->send();
  exit();
}

$return['timestamp'] = date("Y-m-d H:i:s");
$return['id'] = time();
$return['status'] = 'AUTH_FAILED';


function returnFail($code, $reason = '', $request = null) {
  global $return;
  global $db;
  global $token;
  // var_dump($return);
  switch ($code) {
    case 'OK':
      $return['status'] = $code;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
  }
}

// TODO: pastikan panjang field database
// TODO: pastikan fungsi utama file ini
function inputs() {
  return array(
    'code' => v::alnum()->noWhitespace(),
    'access_token' => v::alnum()->noWhitespace(),
  );
}

function validate($input) {
  $c = array();
  $v = inputs();

  foreach (array_keys($v) as $k) {
    if (isset($input[$k])) {
      if ($v[$k]->validate($input[$k])) {
        $c[$k] = $input[$k];
        slog('validasi field '.$k, 'true', 'Format benar', 'success');
      } else {
        //$c[$k] = '';
        slog('validasi field '.$k, 'false', 'Format salah', 'fail');
      }
    }
  }

  return $c;
}

$content = validate($content);

if (isset($content['code'])) {
  $userlogin = $db->query(sprintf("SELECT USER_ID FROM OAUTH_AUTHORIZATION_CODES WHERE AUTHORIZATION_CODE = '%s'", $content['code']));
  if (isset($userlogin[0]['USER_ID'])) {
    $return['username'] = $userlogin[0]['USER_ID'];
    returnFail('OK', '', $content);
  }
} elseif (isset($content['access_token'])) {
  $return['username'] = $token['user_id'];
  returnFail('OK', '', $content);
}
