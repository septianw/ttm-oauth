<?php
// include our OAuth2 Server object
require_once __DIR__.'/server.php';
$db = new Gudang();
require_once __DIR__ . '/vendor/autoload.php';
use Respect\Validation\Validator as v;

$return['timestamp'] = date("Y-m-d H:i:s");
$return['id'] = time();
$return['status'] = null;

$request = OAuth2\Request::createFromGlobals();
$response = new OAuth2\Response();
$content = $request->request;

$token = $server->getAccessTokenData(OAuth2\Request::createFromGlobals());

function slog($request, $retval, $desc, $event) {
  global $db;
  global $token;
  $payload = array(
    'token' => $token,
    'filename' => __FILE__,
    'request' => $request,
    'retval' => $retval,
    'desc' => $desc,
    'event' => $event
  );
  $db->saveLog($payload);
}

// Handle a request for an OAuth2.0 Access Token and send the response to the client
$scopeRequired = 'user';
if (!$server->verifyResourceRequest($request, $response, $scopeRequired)) {
  slog($content, json_encode($response->getParameters()), 'token authorize fail', 'fail');
  // echo "satu";
    $server->getResponse()->send();
    exit();
}

function url_get_contents($Url) {
    if (!function_exists('curl_init')){
        die('CURL is not installed!');
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $Url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}

function returnFail($code, $reason = '', $request = null) {
  global $return;
  global $db;
  global $token;
  // var_dump($return);
  switch ($code) {
    case 'OK':
      $return['status'] = $code;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      if (isset($reason)) {
        $return['reason'] = $reason;
      }
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
    case 'AUTH_FAILED':
      $return['status'] = $code;
      $return['reason'] = $reason;
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
    case 'MSISDN_FAILED':
      $return['status'] = $code;
      if (isset($reason) && (!empty($reason))) {
        $return['reason'] = $reason;
      }
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
    case 'GROUP_FAILED':
      $return['status'] = $code;
      if (isset($reason)) {
        $return['reason'] = $reason;
      }
      header('Content-Type: application/json');
      $payload['retval'] = json_encode($return);
      slog($request, json_encode($return), $reason, $code);
      echo json_encode($return);
    break;
  }
}

function isPenuh($groupName) { // bool
  $return = false;
  global $db;
  
  $gid = gidDariNama($db, $groupName);
  $query = sprintf("SELECT COUNT(*) CAP FROM W2SMS_GROUP_MEMBER A, W2SMS_GROUP B WHERE A.GID = B.GID AND A.GID = %d", $gid);
  $qout = $db->query($query);
  $cap = $qout[0]['CAP'];
  $query = sprintf("SELECT MAX_MEMBER FROM W2SMS_GROUP WHERE GID = %d", $gid);
  $qout = $db->query($query);
  $max = $qout[0]['MAX_MEMBER'];
  
  if ((($cap == $max) || ($cap > $max)) && (!is_null($qout[0]['MAX_MEMBER']))) {
    slog($content, 'group full', sprintf('Group capacity is %d, and number of member is %d.', $max, $cap), 'Try to push more member to group.');
    $return = true;
  }
  
  return $return;
}

function getStatus($db, $msisdn) {
  $query = $db->query(sprintf("
    SELECT STATUS FROM W2SMS_CMR_MEMBER WHERE MSISDN = '%s'",
    $msisdn));
//   var_dump($query, $msisdn); die();

  return $query[0]['STATUS'];
}

function msisdnAda($db, $msisdn) {
  $query = $db->query(sprintf("
    SELECT MSISDN FROM W2SMS_CMR_MEMBER WHERE MSISDN = '%s'",
    $msisdn));
  // var_dump($query);
  if (isset($query[0]['MSISDN'])) {
    return true;
  } else {
    return false;
  }
}

function formatMsisdn($msisdn) {
  if (substr($msisdn, 0, 1) == '8') {
    return $msisdn;
  } elseif (substr($msisdn, 0, 2) == '08') {
    return substr($msisdn, 1);
  } elseif (substr($msisdn, 0, 3) == '628') {
    return substr($msisdn, 2);
  }
}

function midDariMsisdn($db, $msisdn) {
  $mid = $db->query(sprintf("SELECT MID FROM W2SMS_CMR_MEMBER WHERE MSISDN = '%s'", $msisdn));
  if (isset($mid[0]['MID'])) {
    return $mid[0]['MID'];
  } else {
    return false;
  }
}

function gidDariNama($db, $group) {
  $gid = $db->query(sprintf("SELECT GID FROM W2SMS_GROUP WHERE GNAME LIKE '%%%s%%'",strtoupper($group)));
  if (isset($gid[0]['GID'])) {
    return $gid[0]['GID'];
  } else {
    return false;
  }
}

function isMember($msisdn, $gid) {
  global $db; // SELECT A.MSISDN MSISDN FROM W2SMS_GROUP_MEMBER A, W2SMS_GROUP B WHERE A.GID = B.GID AND A.MSISDN = '%s' AND B.GID = 881;
  $query = "SELECT MSISDN FROM W2SMS_GROUP_MEMBER WHERE MSISDN = '%s' AND GID = %d";
  $result = $db->query(sprintf($query, $msisdn, $gid));
//  var_dump(sprintf($query, $msisdn, $gid));
//  var_dump($result); die();
  if (empty($result)) {
    return false;
  } else {
    return true;
  }
}

// susun query
$query = "
  INSERT INTO W2SMS_GROUP_MEMBER (GMID, MID, GID, CREATE_DATE, MSISDN, STATUS)
  VALUES (SEQ_W2SMS_GROUP_MEMBER_ID.NEXTVAL, %d, %d, SYSDATE, '%s', %d)";

function notifyMember($data) /*bool*/ {
  global $db;
  $return = false;
  $message = sprintf('Anda telah diundang untuk bergabung dalam group %s.', $data['group']);
  
//  $idresult = $db->query("SELECT SEQ_SMS_SCHEDULE_SCHEDULEID.NEXTVAL AS INSERT_ID FROM DUAL");
//  $idresult = $idresult[0]['INSERT_ID']+1;
//  $gid = gidDariNama($db, $data['group']);
  
  // production
  //$url = sprintf("http://10.1.89.201:8081/commetmo/SmsMO?channel=WEB&adn=2324&msisdn=62%s&msg=CMBC_MEMBER %d %d", $data['msisdn'], $idresult, $gid);  // TODO production API
  // development
  $url = sprintf("http://10.2.224.148:50003/cgi-bin/sendsms?user=tester&pass=foobar&to=%s&from=2324&text=%s", $data['msisdn'], $message);  // TODO development API
//  $url = sprintf("http://10.2.117.80:8080/commetmo/SmsMO?channel=WEB&adn=2324&msisdn=62%s&msg=CMBC_MEMBER %d %d", $data['msisdn'], $idresult, $gid);  // TODO development API
  
  $url = str_replace(" ", '%20', $url);
  $db->dumpToLog(__LINE__,$url);
  $sent = url_get_contents($url);

  if ($sent > 0) {
    $return = sprintf("CMBC_MEMBER %d %d", $idresult, $gid);
  }
  
  return $return;
}

function inviteToGroup($content) {
  global $db;
  global $query;
  if (isset($content['group'])) {
    if (isset($content['msisdn'])) {
      $content['msisdn'] = formatMsisdn($content['msisdn']);
      if ($mid = midDariMsisdn($db, $content['msisdn'])) {
        if ($gid = gidDariNama($db, $content['group'])) {
          if (!isPenuh($content['group'])) {
//            var_dump(isMember($content['msisdn'], $gid)); die();
            if (!isMember($content['msisdn'], $gid)) {
//              var_dump(getStatus($db, $content['msisdn']));
//              var_dump(sprintf($query, $mid, $gid, $content['msisdn'], getStatus($db, $content['msisdn']))); die();
              if ($db->query(sprintf($query, $mid, $gid, $content['msisdn'], getStatus($db, $content['msisdn'])), false)) {
                $message = '';
                $v = notifyMember($content);
                if ($v !== false) {
                  $message = $v;
                }
                returnFail('OK',$message,$content);
              }
            } else {
              returnFail('MSISDN_FAILED', 'MSISDN exist', $content);
            }
          } else{
            returnFail('GROUP_FAILED', 'This group is full', $content);
          }
        } else {
          returnFail('GROUP_FAILED','',$content);
        }
      } else {
        returnFail('MSISDN_FAILED', '', $content);
      }
    } else {
      returnFail('MSISDN_FAILED', '', $content);
    }
  } else {
    returnFail('GROUP_FAILED','',$content);
  }
}

function inputs() {
  return array(
    'msisdn' => v::notEmpty()->numeric()->positive()->length(9,20),
    'group' => v::alnum()->noWhitespace()->length(1,20)
  );
}

function validate($input) {
  $c = array();
  $v = inputs();

  foreach (array_keys($v) as $k) {
    if (isset($input[$k])) {
      if ($v[$k]->validate($input[$k])) {
        $c[$k] = $input[$k];
        slog('validasi field '.$k, 'true', 'Format benar', 'success');
      } else {
        //$c[$k] = '';
        slog('validasi field '.$k, 'false', 'Format salah', 'fail');
      }
    }
  }

  return $c;
}

$content = validate($content);

// cek login dulu baru insert
// NOTE: 14 march 2017: no more username check
inviteToGroup($content);
